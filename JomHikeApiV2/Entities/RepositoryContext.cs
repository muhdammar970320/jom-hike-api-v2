﻿using Entities;
using Entities.Models;
using JomHikeApiV2.Entities;
using JomHikeApiV2.Entities.Configuration;
using JomHikeApiV2.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
    public class RepositoryContext:DbContext
    {
        public RepositoryContext(DbContextOptions<RepositoryContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new VehicleConfiguration());
            modelBuilder.ApplyConfiguration(new MountainConfiguration());
            modelBuilder.ApplyConfiguration(new MountainReviewConfiguration());
            modelBuilder.ApplyConfiguration(new EventConfiguration());
            modelBuilder.ApplyConfiguration(new RideConfiguration());
            modelBuilder.ApplyConfiguration(new RequestConfiguration());
            modelBuilder.ApplyConfiguration(new UserEventConfiguration());
        }
        //Register the entity class at here.
        public DbSet<User> Users { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<MountainReview> MountainReviews { get; set; }
        public DbSet<Mountain> Mountains { get; set; }
        public DbSet<Request> Requests { get; set; }
        public DbSet<Ride> Rides { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<UserEvent> UserEvents { get; set; }

        //public DbSet<VechicleUser> VehicleUsers { get; set; }
    }
}
