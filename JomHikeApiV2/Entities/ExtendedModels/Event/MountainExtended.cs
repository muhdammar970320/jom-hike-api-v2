﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.Entities.ExtendedModels.Event
{
    public class MountainExtended
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public Double DestinationLongitude { get; set; }
        public Double DestinationLatitude
        {
            get; set;
        }
    }
}
