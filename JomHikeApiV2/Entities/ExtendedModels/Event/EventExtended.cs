﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.Entities
{
    public class EventExtended
    {
  
        public int Id { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public string State { get; set; }
        public string OrganizerName { get; set; }
        public string OrganizerRegistrationNumber { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Status { get; set; } = "Pending";
        public Guid UserId { get; set; }
        public User User { get; set; }
        public string EventImageUrl { get; set; }
        //public int MountainId { get; set; }
        public Mountain Mountain { get; set; }

        public EventExtended()
        {
        }
    }
}
