﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.Entities
{
    public class VehicleExtended
    {

        public int Id { get; set; }

        public string Name { get; set; }

        public string Brand { get; set; }

        public int? MakeYear { get; set; }

        public string Color { get; set; }

        public string PlateNumber { get; set; }

        public UserVehicleExtended User { get; set; }

        public VehicleExtended()
        {
    
        }

    }
}
