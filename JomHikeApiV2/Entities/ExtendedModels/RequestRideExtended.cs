﻿using Entities.Models;
using JomHikeApiV2.Entities.ExtendedModels.Event;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.Entities.ExtendedModels
{
    public class RequestRideExtended
    {
        public int Id { get; set; }
        public int SeatAvailable { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime RideStartTime { get; set; }
        public Double DestinationLongitude { get; set; }
        public Double DestinationLatitude { get; set; }
        public string Status { get; set; }
        public double TotalDistance { get; set; }
        public double TotalCost { get; set; }
        public double FinalTotalCost { get; set; }
        public ICollection<RequestExtended> Requests { get; set; }
        public MountainExtended Mountain { get; set; }
        public VehicleExtended vehicle { get; set; }
        public RequestRideExtended()
        {
        }
    }
}
