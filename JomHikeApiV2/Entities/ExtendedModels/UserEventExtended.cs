﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.Entities.ExtendedModels
{
    public class UserEventExtended
    {

        public int Id { get; set; }
        public int IsRequestRide { get; set; }
        public int IsOfferRide { get; set; }
        public int MaxAttempt { get; set; } 
        public string Status { get; set; }
        public Guid UserId { get; set; }
        public int EventId { get; set; }
    }
}
