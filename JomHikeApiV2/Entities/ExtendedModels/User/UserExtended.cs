﻿
using Entities.Models;
using JomHikeApiV2.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.Entities
{
    public class UserExtended
    {
        public Guid Id { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string Salt { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string IdentityNumber { get; set; }

        public string ProfileImageUrl { get; set; }

        public string ProfileImageBase64 { get; set; }

        public string PhoneNumber { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string PostalCode { get; set; }
        public string Role { get; set; }

        public string Email { get; set; }

        public int Status { get; set; }

        public string DriverLicenseNumber { get; set; }

        public DateTime DriverLicenseExpiredDate { get; set; } = DateTime.Now.AddMonths(12);

        public IEnumerable<Event> Events { get; set; }

        public IEnumerable<UserEvent> UserEvents { get; set; }

        public IEnumerable<Vehicle> Vehicles { get; set; }

        public IEnumerable<MountainReview> MountainReviews { get; set; }

        public IEnumerable<Request> Requests { get; set; }

        public string UserIcFrontImageBase64 { get; set; }

        public string UserIcFrontImageUrl { get; set; }

        public string UserIcBackImageBase64 { get; set; }

        public string UserIcBackImageUrl { get; set; }

        public string State { get; set; }

        public IEnumerable<Ride> Rides { get; set; }

        public string UserSelfieImageBase64 { get; set; }

        public string UserSelfieImageUrl { get; set; }

        public UserExtended()
        {
        }
        public UserExtended(User user)
        {
            Id = user.Id;
            UserName = user.UserName;
            FirstName = user.FirstName;
            LastName = user.LastName;
            IdentityNumber = user.IdentityNumber;
            PhoneNumber = user.PhoneNumber;
            Address1 = user.Address1;
            Address2 = user.Address2;
            PostalCode = user.PostalCode;
            Email = user.Email;
            Status = user.Status;
            DriverLicenseExpiredDate = user.DriverLicenseExpiredDate;
            DriverLicenseNumber = user.DriverLicenseNumber;
            UserIcBackImageUrl = user.UserIcBackImageUrl;
            UserIcFrontImageUrl = user.UserIcFrontImageUrl;
            UserSelfieImageUrl = user.UserSelfieImageUrl;
            //Token = "sdada";
        }
       
    }
}
