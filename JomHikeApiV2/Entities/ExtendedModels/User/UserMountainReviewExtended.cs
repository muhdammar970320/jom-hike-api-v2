﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.Entities
{
    public class UserMountainReviewExtended
    {
        public string UserName { get; set; }
        public UserMountainReviewExtended()
        {
        }
        public UserMountainReviewExtended(User user)
        {
            UserName = user.UserName;
        }
    }
}
