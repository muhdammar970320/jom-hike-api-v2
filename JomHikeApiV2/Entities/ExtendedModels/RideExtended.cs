﻿
using JomHikeApiV2.Entities.Models;
using JomHikeApiV2.Entities.ExtendedModels.Event;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities.Models;

namespace JomHikeApiV2.Entities
{
    public class RideExtended
    {
 
        public int Id { get; set; }
        public DateTime RideStartTime { get; set; }
        public Double DestinationLongitude { get; set; }
        public Double DestinationLatitude { get; set; }
        public int SeatAvailable { get; set; }
        public Double TotalCost { get; set; } = 0;
        public Double TotalDistance { get; set; } = 0;
        //public int TollFee { get; set; }
        //public float CostPerHead { get; set; }

        public DateTime CreatedAt { get; set; }
        //public TimeSpan TimePickup { get; set; }
        //public DateTime DatePickup { get; set; }

        //Relationship 
        //Ride has One Vehicle
        public VehicleExtended Vehicle { get; set; }
        public UserExtended User { get; set; }
        public EventRideExtended Event { get; set; }
        public RideExtended()
        {
        }
    }
}
