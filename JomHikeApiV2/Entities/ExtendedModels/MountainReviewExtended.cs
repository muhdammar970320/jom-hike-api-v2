﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.Entities.ExtendedModels
{
    public class MountainReviewExtended
    {
        public int Id { get; set; }

        public string ReviewDescription { get; set; }

        public DateTime ReviewDate { get; set; } = DateTime.Now;

        public Double MountainDifficulty { get; set; } = 0;

        public Double MountainRating { get; set; } = 0;

        //Relationship
        public int MountainId { get; set; }
        public Guid UserId { get; set; }
        public Mountain Mountain { get; set; }
        public UserMountainReviewExtended User { get; set; }
    }
}
