﻿using JomHikeApiV2.Entities.ExtendedModels.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.Entities.ExtendedModels
{
    public class RequestPaymentExtended
    {

        public int Id { get; set; }
        public string PaymentStatus { get; set; }
        public int PaymentMethod { get; set; }
    }
}
