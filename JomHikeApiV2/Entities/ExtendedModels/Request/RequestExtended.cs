﻿using JomHikeApiV2.Entities.ExtendedModels.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.Entities.ExtendedModels
{
    public class RequestExtended
    {

        public int Id { get; set; }
        public Double PickupLongitude { get; set; }
        public Double PickupLatitude { get; set; }
        public Double? EstimatedCost { get; set; }
        public Double? FinalTotalCost { get; set; }
        public Double? TotalDistance { get; set; }
        public Double? FinalTotalDistance { get; set; }
        public Double? Rating { get; set; } 
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public string Status { get; set; }
        public UserRequestRideExtended User { get; set; }
    }
}
