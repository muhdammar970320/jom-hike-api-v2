﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.Entities.ExtendedModels.Request
{
    public class RideWithDriverDetailsExtended
    {
        public int Id { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String MountainName { get; set; }
        public DateTime RideStartTime { get; set; }
        public double DestinationLongitude { get; set; }
        public double DestinationLatitude { get; set; }
        public int SeatAvailable { get; set; }
        public Double TotalCost { get; set; } = 0;
        public Double TotalDistance { get; set; } = 0;
        public VehicleExtended Vehicle { get; set; }
        public RideWithDriverDetailsExtended()
        {
        }
    }
}
