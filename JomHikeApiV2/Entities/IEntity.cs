﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Utils
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
