﻿using Entities.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    public class Mountain :IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Name", Prompt = "Gunung Kinabalu")]
        [MaxLength(30, ErrorMessage = "Mountain Name exceeds 30 characters")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Height in Feet", Prompt = "5323")]
        public float HeightFeet { get; set; }

        [Required]
        [Display(Name = "Height in Meter", Prompt = "2321")]
        public float HeightMeter { get; set; }

        [Required]
        public string State { get; set; }

        public Double AverageDifficulty { get; set; } = 0;
        public Double AverageRating { get; set; } = 0;
        [Required]
        [Display(Name = "Longitude", Prompt = "23.421")]
        public Double DestinationLongitude { get; set; }
        [Required]
        [Display(Name = "Latitude", Prompt = "123.34")]
        public Double DestinationLatitude { get; set; }

        public string Status { get; set; } = "Pending";
        //Relationship
        public ICollection<MountainReview> MountainReviews { get; set; }
        //public Event Event { get; set; }

    }
}
