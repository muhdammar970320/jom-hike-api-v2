﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.Entities.Models
{
    public class UserEvent
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int IsRequestRide { get; set; } = 0;
        public int IsOfferRide { get; set; } = 0;
        public int MaxAttempt { get; set; } = 0;
        public string Status { get; set; } = "Joined";
        public Guid UserId { get; set; }
        public User User { get; set; }

        public int EventId { get; set; }
        public Event Event { get; set; }

    }
}
