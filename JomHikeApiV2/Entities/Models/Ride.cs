﻿using Entities.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    public class Ride : IEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public DateTime RideStartTime { get; set; }
        public Double DestinationLongitude { get; set; }
        public Double DestinationLatitude { get; set; }
        public int SeatAvailable { get; set; }
        public Double TotalCost { get; set; } = 0;
        public Double TotalDistance { get; set; } = 0;
        public Double FinalTotalCost { get; set; } = 0;

        public string Status { get; set; } = "Ongoing";
        //public int TollFee { get; set; }
        //public float CostPerHead { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime CreatedAt { get; set; } =  DateTime.Now;
        //public TimeSpan TimePickup { get; set; }
        //public DateTime DatePickup { get; set; }

        //Relationship 
        //Ride has One Vehicle
        public int VehicleId { get; set; }
        public Vehicle Vehicle { get; set; }

        //Ride has Many Request
        public ICollection<Request> Requests { get; set; }

        public int EventId { get; set; }
        public Event Event { get; set; }


    }
}
