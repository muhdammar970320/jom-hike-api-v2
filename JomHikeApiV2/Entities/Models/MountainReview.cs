﻿using Entities.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    public class MountainReview : IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public string ReviewDescription { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime ReviewDate { get; set; } = DateTime.Now;

        public Double MountainDifficulty { get; set; } = 0;
        public Double MountainRating { get; set; } = 0;

        //Relationship
        public int MountainId { get; set; }
        public Guid UserId { get; set; }
        public Mountain Mountain { get; set; }
        public User User { get; set; }
    }
}
