﻿using JomHikeApiV2.Entities.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
   public class User : IUserEntity
    {
        [Key]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [MaxLength(15, ErrorMessage = "Username exceed 15 characters")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [MaxLength(250, ErrorMessage = "Password exceed 15 characters")]
        public string Password { get; set; }

        [MaxLength(250)]
        public string Salt { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "First Name exceed 30 characters")]
        [Display(Name = "First Name", Prompt = "Muhammad Ammar")]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "Last Name exceed 30 characters")]
        [Display(Name = "Last Name", Prompt = "Abdul Aziz")]
        public string LastName { get; set; }

        //[Required]/
        [Display(Name = "No IC", Prompt = "970320-10-7777")]
        [RegularExpression(@"^\d{6}\-\d{2}\-\d{4}$", ErrorMessage = "The {0} does not meet requirements.")]
        public string IdentityNumber { get; set; }

        public string ProfileImageUrl { get; set; }
        
        [NotMapped]
        public string ProfileImageBase64 { get; set; }

        public string PhoneNumber { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string PostalCode { get; set; }
        public string Role { get; set; } = "User";
        public string State { get; set; }

        //public byte ProfileImage { get; set; }
        //public byte IdentityImage { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        // 0 - Pending
        // 1 - Updated
        // 2 - Verified
        // 3 - Rejected
        // 4 - Pending again, need to verify again.
        [DefaultValue(0)]
        public int Status { get; set; }
        public string DriverLicenseNumber { get; set; }
        public DateTime DriverLicenseExpiredDate { get; set; } = DateTime.Now.AddMonths(12);

        public ICollection<Vehicle> Vehicles { get; set; }
        public ICollection<Event> Events { get; set; }
        public ICollection<MountainReview> Reviews { get; set; }
        public ICollection<Request> Requests { get; set; }

        public ICollection<UserEvent> UserEvents { get; set; }

        [NotMapped]
        public string UserIcFrontImageBase64 { get; set; }

        public string UserIcFrontImageUrl { get; set; }

        [NotMapped]
        public string UserIcBackImageBase64 { get; set; }

        public string UserIcBackImageUrl { get; set; }

        [NotMapped]
        public string UserSelfieImageBase64 { get; set; }

        public string UserSelfieImageUrl { get; set; }

        //ICollection<VehicleUser>
    }
}
