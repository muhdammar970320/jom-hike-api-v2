﻿using Entities.Utils;
using JomHikeApiV2.Entities.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    public class Event : IEntity
    {
        //public int MountainId { get; set; }
        //public Guid OrganizerByUserId { get; set; }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "Event Name exceed 50 characters")]
        public string Name { get; set; }

        [Required]
        public string Location { get; set; }

        [Required]
        [MaxLength(20, ErrorMessage = "State Name exceed 20 characters")]
        public string State { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "Organizer Name exceed 50 characters")]
        public string OrganizerName { get; set; }

        [Required]
        [MaxLength(10, ErrorMessage = "Register Number More than 10 characters")]
        public string OrganizerRegistrationNumber { get; set; }
        public string Description { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        [NotMapped]
        public string EventImageBase64 { get; set; }

        public string EventImageUrl { get; set; }

        [Required]
        public DateTime EndDate { get; set; }
        public string Status { get; set; } = "Pending";

        //Relationship
        public Guid UserId { get; set; }
        public User User { get; set; }
        public int MountainId { get; set; }
        public Mountain Mountain { get; set; }
        public ICollection<Ride> Rides { get; set; }

        public ICollection<UserEvent> UserEvents { get; set; }

    }
}
