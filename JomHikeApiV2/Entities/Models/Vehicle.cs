﻿using Entities.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    public class Vehicle : IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public string Brand { get; set; }

        [Range(1,2050,ErrorMessage ="You have input wrong year number")]
        public int? MakeYear { get; set; }

        public string Color { get; set; }

        public string PlateNumber { get; set; }
        //public Guid? UserId { get; set; }
        public Guid UserId { get; set; }
        public User User { get; set; }


        //public string OwnerName { get; set; }

        //public string GrantImage { get; set; }

        //public string IcImage { get; set; }
        public ICollection<Ride> Rides { get; set; }
    }
}
