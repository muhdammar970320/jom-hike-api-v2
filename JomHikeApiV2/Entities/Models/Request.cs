﻿using Entities.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    public class Request : IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public Double PickupLongitude { get; set; }

        [Required]
        public Double PickupLatitude { get; set; }


        public Double? EstimatedCost { get; set; } = 0;
        public Double? TotalDistance { get; set; } = 0;

        public Double? FinalTotalCost { get; set; } = 0;
        public Double? FinalTotalDistance { get; set; } = 0;
        public Double? Rating { get; set; } = 0;

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        //To Do: Change by StatusId(RequestStatus) in next features 
        public string Status { get; set; } = "Pending";
        //public int RideId { get; set; }
        public string PaymentStatus { get; set; } = "Not Paid Yet";
        public int PaymentMethod { get; set; } = 0;

        //Relationship 
        public Guid UserId { get; set; }


        public User User { get; set; }

        public int RideId { get; set; }


        public Ride Ride { get; set; }

        //======different way to insert
        //public Guid UserId { get; set; }
        //[ForeignKey("UserId")]
        //public User User { get; set; }

        //public int RideId { get; set; }

        //[ForeignKey("RideId")]
        //public Ride Ride { get; set; }

    }
}
