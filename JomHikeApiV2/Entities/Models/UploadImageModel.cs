﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.Entities.Models
{
    public class UploadImageModel
    {
        public string Description { get; set; }
        public string ImageData { get; set; }
    }
}
