﻿using Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.Entities.Configuration
{
    public class EventConfiguration : IEntityTypeConfiguration<Event>
    {
        public void Configure(EntityTypeBuilder<Event> builder)
        {
            builder.Property(e => e.Id)
            .ValueGeneratedOnAdd();
            builder.ToTable("Event");
            builder.HasData
           (
               new
               {
                   Id = -1,
                   UserId = Guid.Parse("742d5cae-6184-4a32-a58d-36e23febf1a3"),//Hazman
                   Name = "XPDC ke Gunung Angsi",
                   Location = "Bukit Putus",
                   State = "Negeri Sembilan",
                   OrganizerName = "UiTM Seremban",
                   OrganizerRegistrationNumber = "ABC123",
                   Description = "Hiking Gunung Angsi",
                   StartDate = DateTime.Now.AddMonths(1),
                   EndDate = DateTime.Now.AddMonths(1).AddDays(1),
                   Status = "Pending",
                   MountainId = -1,//Gunung Angsi
                   EventImageUrl = "default.jpg"
               },
               new
               {
                   Id = -2,
                   UserId = Guid.Parse("d923e0af-c142-4ce2-bdfa-142e6d0c6cf5"),//Nik
                   Name = "XPDC ke Gunung Datuk",
                   Location = "Rembau",
                   State = "Negeri Sembilan",
                   OrganizerName = "USIM",
                   OrganizerRegistrationNumber = "ABC1232",
                   Description = "Hiking Gunung Datuk",
                   StartDate = DateTime.Now.AddMonths(2),
                   EndDate = DateTime.Now.AddMonths(2).AddDays(1),
                   Status = "Pending",
                   MountainId = -1,//Gunung Datuk
                   EventImageUrl = "default.jpg"
               }
            );
        }
    }
}
