﻿using JomHikeApiV2.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.Entities.Configuration
{
    public class UserEventConfiguration : IEntityTypeConfiguration<UserEvent>
    {
        public void Configure(EntityTypeBuilder<UserEvent> builder)
        {
            builder.HasKey(ue => new { ue.UserId, ue.EventId });

            builder.HasOne(ue => ue.User)
                .WithMany(u => u.UserEvents)
                .HasForeignKey(ue => ue.UserId);

            builder.HasOne(ue => ue.Event)
                .WithMany(u => u.UserEvents)
                .HasForeignKey(ue => ue.EventId);

            builder.ToTable("UserEvent");
            builder.HasData
          (
               new
               {
                   Id = -1,
                   EventId = -1,
                   IsOfferRide = 0,
                   IsRequestRide = 0,
                   MaxAttempt = 0,
                   Status = "Joined",
                   UserId = Guid.Parse("d923e0af-c142-4ce2-bdfa-142e6d0c6cf5"),//Nik
               },
                new
                {
                    Id = -2,
                    EventId = -2,
                    IsOfferRide = 0,
                    IsRequestRide = 0,
                    MaxAttempt = 0,
                    Status = "Joined",
                    UserId = Guid.Parse("d923e0af-c142-4ce2-bdfa-142e6d0c6cf5"),//Nik
                },
                 new
                 {
                     Id = -3,
                     EventId = -2,
                     IsOfferRide = 0,
                     IsRequestRide = 0,
                     MaxAttempt = 0,
                     Status="Joined",
                     UserId = Guid.Parse("742d5cae-6184-4a32-a58d-36e23febf1a3"),//Hazman
                 }
                 );
        }
    }
}
