﻿using Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.Entities.Configuration
{
    class VehicleConfiguration : IEntityTypeConfiguration<Vehicle>
    {
        public void Configure(EntityTypeBuilder<Vehicle> builder)
        {
            builder.Property(v => v.Id)
           .ValueGeneratedOnAdd();
            builder.ToTable("Vehicles");
            builder.HasData
           (
                new
                {
                    Id = -1,
                    UserId = Guid.Parse("742d5cae-6184-4a32-a58d-36e23febf1a3"),//Hazman
                    Name = "Axia",
                    Brand = "Perodua",
                    Color = "Yello",
                    MakeYear = 2017,
                    PlateNumber = "ABC125"
                },

            new
            {
                Id = -2,
                UserId = Guid.Parse("742d5cae-6184-4a32-a58d-36e23febf1a3"),//Hazman
                Name = "Viva",
                Brand = "Produa",
                Color = "Red",
                MakeYear = 2017,
                PlateNumber = "ABC123"
            },
           new
           {
               Id = -3,
               UserId = Guid.Parse("d923e0af-c142-4ce2-bdfa-142e6d0c6cf5"),//Nik
               Name = "Myvi",
               Brand = "Perodua",
               Color = "Red",
               MakeYear = 2014,
               PlateNumber = "ABC124"

           }



           );


        }
    }

}
