﻿using Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.Entities
{
    public class RideConfiguration : IEntityTypeConfiguration<Ride>
    {
        public void Configure(EntityTypeBuilder<Ride> builder)
        {
            builder.Property(r => r.Id)
           .ValueGeneratedOnAdd();
            builder.ToTable("Ride");
            builder.HasData
          (
               new
               {
                   Id = -1,
                   RideStartTime = DateTime.Now.AddDays(9).AddHours(9),
                   DestinationLongitude = 101.906730,
                   DestinationLatitude = 2.653410,
                   SeatAvailable = 4,
                   TotalCost = 100.00,
                   TotalDistance = 50.5,
                   FinalTotalCost = 50.5,
                   Status = "Ongoing",
                   VehicleId = -1,//Axia //Hazman
                   EventId = -1,//Gunung Angsi
                   CreatedAt = DateTime.Now

               },
                new
                {
                    Id = -2,
                    RideStartTime = DateTime.Now.AddDays(18).AddHours(22),
                    DestinationLongitude = 101.919270,
                    DestinationLatitude = 2.652990,
                    SeatAvailable = 4,
                    TotalCost = 80.00,
                    TotalDistance = 67.5,
                    FinalTotalCost = 50.5,
                    Status = "Ongoing",
                    VehicleId = -2,//Viva //Hazman
                    EventId = -2,//Gunung Datuk
                    CreatedAt = DateTime.Now

                },
                 new
                 {
                     Id = -3,
                     RideStartTime = DateTime.Now.AddDays(5).AddHours(5),
                     DestinationLongitude = 101.380142,
                     DestinationLatitude = 4.472120,
                     SeatAvailable = 4,
                     TotalCost = 70.00,
                     TotalDistance = 67.5,
                     FinalTotalCost = 50.5,
                     Status = "Ongoing",
                     VehicleId = -3,//Myvi //Nik
                     EventId = -2,//Gunung Datuk
                     CreatedAt = DateTime.Now

                 }

          );
        }
    }
}
