﻿using Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.Entities.Configuration
{
    public class MountainConfiguration : IEntityTypeConfiguration<Mountain>
    {
        public void Configure(EntityTypeBuilder<Mountain> builder)
        {
            builder.Property(m => m.Id)
            .ValueGeneratedOnAdd();
            builder.ToTable("Mountain");
            builder.HasData
           (
                new Mountain
                {
                    Id = -1,
                    Name = "Gunung Angsi",
                    HeightFeet = 2702,
                    HeightMeter = 824,
                    State = "Negeri Sembilan",
                    DestinationLatitude = 2.6986,
                    DestinationLongitude = 102.0481,

                },
                 new Mountain
                 {
                     Id = -2,
                     Name = "Gunung Datuk",
                     HeightFeet = 2900,
                     HeightMeter = 884,
                     State = "Negeri Sembilan",
                     DestinationLatitude = 2.5435,
                     DestinationLongitude = 102.1691,

                 },
                  new Mountain
                  {
                      Id = -3,
                      Name = "Gunung Yong Belar",
                      HeightFeet = 7162,
                      HeightMeter = 2181,
                      State = "Perak",
                      DestinationLatitude = 4.6500,
                      DestinationLongitude = 101.3617,

                  }

           );
        }
    }
}
