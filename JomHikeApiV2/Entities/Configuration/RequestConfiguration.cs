﻿using Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.Entities.Configuration
{
    public class RequestConfiguration : IEntityTypeConfiguration<Request>
    {
        public void Configure(EntityTypeBuilder<Request> builder)
        {
            builder.Property(r => r.Id)
           .ValueGeneratedOnAdd();
            builder.ToTable("Request");
            builder.HasData
          (
               new
               {
                   Id = -1,
                   PickupLongitude = 101.518578,
                   PickupLatitude = 3.074150,
                   EstimatedCost = 32.0,
                   CreatedAt = DateTime.Now,
                   Status = "Pending",
                   UserId = Guid.Parse("d923e0af-c142-4ce2-bdfa-142e6d0c6cf5"),//Nik
                   RideId = -1,
                   PaymentMethod = 0,
                   PaymentStatus = "Not Paid Yet"


               },
                new
                {
                    Id = -2,
                    PickupLongitude = 101.518578,
                    PickupLatitude = 3.074150,
                    EstimatedCost = 40.2,
                    CreatedAt = DateTime.Now,
                    Status = "Pending",
                    UserId = Guid.Parse("d923e0af-c142-4ce2-bdfa-142e6d0c6cf5"),//Nik
                    RideId = -2,
                    PaymentMethod = 0,
                    PaymentStatus = "Not Paid Yet"

                },
                 new
                 {
                     Id = -3,
                     PickupLongitude = 101.775192,
                     PickupLatitude = 2.888800,
                     EstimatedCost = 40.2,
                     CreatedAt = DateTime.Now,
                     Status = "Pending",
                     UserId = Guid.Parse("742d5cae-6184-4a32-a58d-36e23febf1a3"),//Hazman
                     RideId = -3,
                     PaymentMethod = 0,
                     PaymentStatus = "Not Paid Yet"

                 }
                 );
        }


    }
}
