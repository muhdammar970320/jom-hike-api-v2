﻿using Entities;
using Entities.Models;
using Entities.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JomHikeApiV2.Entities.Configuration
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public RepositoryContext context;
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("Users");
            builder.Property(s => s.Salt)
                .IsRequired(false);
            //builder.Property(s => s.IsRegularStudent)
            //    .HasDefaultValue(true);
            var HashSalt = Convert.ToBase64String(Common.GetRandomSalt(16));
            var HashPassword = Convert.ToBase64String(Common.SaltHashPassword(
                            Encoding.ASCII.GetBytes("sayang"),
                            Convert.FromBase64String(HashSalt)));

            //var userId1 = Guid.NewGuid();
            //var userId2 = Guid.NewGuid();
            //var userId3 = Guid.NewGuid();


            //var UserWithVehicle = new List<User>()
            //{

            //};
            //context.Users.AddRange(UserWithVehicle);
            //    context.SaveChanges();

            builder.HasData(
                  new User()
                  {
                      Id = Guid.Parse("460ed2fc-04b0-4431-ad50-e92b68a7d053"),
                      UserName = "ammar",
                      Password = HashPassword,
                      Salt = HashSalt,
                      FirstName = "Muhammad Ammar",
                      LastName = "Abdul Aziz",
                      IdentityNumber = "970320-10-9999",
                      PhoneNumber = "0172605960",
                      Address1 = "No 1, Jalan 2",
                      Address2 = "Klang, Selangor",
                      PostalCode = "42100",
                      Email = "zkop78@gmail.com",
                      Status = 1,
                      Role ="Admin",
                      DriverLicenseNumber = "123456789",
                      ProfileImageUrl = "default.jpg",
                      UserIcBackImageUrl = null,
                      UserIcFrontImageUrl = null,
                      UserSelfieImageUrl = null,
                      State = "N9"

                      //    Vehicles = {
                      //    Vehicle_1

                      //    //DriverLicenseExpiredDate = DateTime.Now.AddMonths(12), // add 1 year forward from now
                      //}
                  },
                    new User()
                    {
                        Id = Guid.Parse("742d5cae-6184-4a32-a58d-36e23febf1a3"),
                        UserName = "hazman",
                        Password = HashPassword,
                        Salt = HashSalt,
                        FirstName = "Ahmad Hazman",
                        LastName = "Khairulanuar",
                        IdentityNumber = "970320-10-9998",
                        PhoneNumber = "0172605960",
                        Address1 = "No 1, Jalan 2",
                        Address2 = "Klang, Selangor",
                        PostalCode = "42100",
                        Email = "amanmos97@gmail.com",
                        Status = 1,
                        DriverLicenseNumber = "123456789",
                        ProfileImageUrl = "default.jpg",
                        UserIcBackImageUrl = null,
                        UserIcFrontImageUrl = null,
                        UserSelfieImageUrl = null,
                        State = "N9"
                        //Vehicles =
                        //{
                        //  Vehicle_2
                        //}
                        //DriverLicenseExpiredDate = DateTime.Now.AddMonths(12), // add 1 year forward from now
                    },
                        new User()
                        {
                            Id = Guid.Parse("d923e0af-c142-4ce2-bdfa-142e6d0c6cf5"),
                            UserName = "nik",
                            Password = HashPassword,
                            Salt = HashSalt,
                            FirstName = "Nik Farhan",
                            LastName = "Abdullah",
                            IdentityNumber = "970320-10-9997",
                            PhoneNumber = "0172605960",
                            Address1 = "No 1, Jalan 2",
                            Address2 = "Klang, Selangor",
                            PostalCode = "41300",
                            Email = "nik97@gmail.com",
                            Status = 0,
                            DriverLicenseNumber = "123456789",
                            ProfileImageUrl = "default.jpg",
                            UserIcBackImageUrl = null,
                            UserIcFrontImageUrl = null,
                            UserSelfieImageUrl = null,
                            State = "N9"
                            //Vehicles =
                            //{
                            //  Vehicle_3
                            // }
                        }
                       //DriverLicenseExpiredDate = DateTime.Now.AddMonths(12), // add 1 year forward from now
                       );
        }
    }
}
