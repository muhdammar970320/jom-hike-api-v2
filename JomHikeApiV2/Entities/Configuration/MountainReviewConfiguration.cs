﻿using Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.Entities.Configuration
{
    public class MountainReviewConfiguration : IEntityTypeConfiguration<MountainReview>
    {
        public void Configure(EntityTypeBuilder<MountainReview> builder)
        {
            builder.Property(mr => mr.Id)
            .ValueGeneratedOnAdd();
            builder.ToTable("MountainReview");
            builder.HasData
           (
                new
                {

                    Id = -1,
                    MountainId = -1, //Gunung Angsi
                    ReviewDescription = "Nice View, Suitable for beginner",
                    MountainDifficulty = 2.0,
                    MountainRating = 5.0,
                    ReviewDate = DateTime.Now,
                    UserId = Guid.Parse("d923e0af-c142-4ce2-bdfa-142e6d0c6cf5") //Nik

                },
                 new
                 {
                     Id = -2,
                     MountainId = -1, //Gunung Angsi
                     ReviewDescription = "Challenging, Suitable for training",
                     MountainDifficulty = 4.0,
                     MountainRating = 4.0,
                     ReviewDate = DateTime.Now,
                     UserId = Guid.Parse("742d5cae-6184-4a32-a58d-36e23febf1a3")//Hazman

                 },
                  new
                  {
                      Id = -3,
                      MountainId = -2,//Gunung Datuk
                      ReviewDescription = "Too easy, for professional",
                      MountainDifficulty = 4.0,
                      MountainRating = 1.0,
                      ReviewDate = DateTime.Now,
                      UserId = Guid.Parse("460ed2fc-04b0-4431-ad50-e92b68a7d053") //Ammar

                  },
                   new
                   {
                       Id = -4,
                       MountainId = -3,//Gunung YB
                       ReviewDescription = "Challenging, Not Suitable for training",
                       MountainDifficulty = 5.0,
                       MountainRating = 2.0,
                       ReviewDate = DateTime.Now,
                       UserId = Guid.Parse("d923e0af-c142-4ce2-bdfa-142e6d0c6cf5") //Nik

                   }
               );



        }
    }
}
