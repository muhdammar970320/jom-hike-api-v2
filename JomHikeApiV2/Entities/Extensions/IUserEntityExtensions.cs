﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Extensions
{
    public static class IUserEntityExtensions
    {
        public static bool IsObjectNull(this IUserEntity entity)
        {
            return entity == null;
        }
        public static bool IsEmptyObject(this IUserEntity entity)
        {
            return entity.Id.Equals(Guid.Empty);
        }
    }
}
