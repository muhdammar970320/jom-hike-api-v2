﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Extensions
{
    public static class RideExtensions
    {
        public static void Map(this Ride dbRide, Ride ride)
        {
            dbRide.RideStartTime = ride.RideStartTime;
            dbRide.DestinationLatitude = ride.DestinationLatitude;
            dbRide.DestinationLongitude = ride.DestinationLongitude;
            dbRide.VehicleId = ride.VehicleId;
            dbRide.TotalCost = ride.TotalCost;
            dbRide.SeatAvailable = ride.SeatAvailable;
            dbRide.TotalDistance = ride.TotalDistance;
        }

        public static void MapStatus(this Ride dbRide, string status)
        {
            dbRide.Status = status;
        }
    }
}
