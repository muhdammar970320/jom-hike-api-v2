﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.Entities.Extensions
{
    public static class RequestExtensions
    {
        public static void Map(this Request dbRequest, Request request)
        {
            dbRequest.PickupLatitude = request.PickupLatitude;
            dbRequest.PickupLongitude = request.PickupLongitude;
            dbRequest.EstimatedCost = request.EstimatedCost;
          
        }
        public static void CancelRequestMap(this Request dbRequest, Request request)
        {
            //This wrong way to update the status
            //The correct way is not need to update latitude and longitude
            //but it can be error since latitude and longitude is required
            //Fix it later
            dbRequest.PickupLatitude = request.PickupLatitude;
            dbRequest.PickupLongitude = request.PickupLongitude;
            dbRequest.Status = request.Status;
        }
        public static void ApproveRequestMap(this Request dbRequest, Request request)
        {
            //This wrong way to update the status
            //The correct way is not need to update latitude and longitude
            //but it can be error since latitude and longitude is required
            //Fix it later
            dbRequest.PickupLatitude = request.PickupLatitude;
            dbRequest.PickupLongitude = request.PickupLongitude;
            dbRequest.Status = request.Status;
        }
        public static void PaymentMap(this Request dbRequest, Request request)
        {
            //This wrong way to update the status
            //The correct way is not need to update latitude and longitude
            //but it can be error since latitude and longitude is required
            //Fix it later
            dbRequest.PaymentMethod = request.PaymentMethod;
            dbRequest.PaymentStatus = "Paid";
        }
    }
}
