﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Extensions
{
   public static class UserExtensions
    {
        public static void Map(this User dbUser, User user)
        {
           
            dbUser.FirstName = user.FirstName;
            dbUser.LastName = user.LastName;
            dbUser.IdentityNumber = user.IdentityNumber;
            dbUser.PhoneNumber = user.PhoneNumber;
            dbUser.Address1 = user.Address1;
            dbUser.Address2 = user.Address2;
            dbUser.PostalCode = user.PostalCode;
            dbUser.Email = user.Email;
            dbUser.Role = user.Role;
            dbUser.DriverLicenseNumber = user.DriverLicenseNumber;
            dbUser.DriverLicenseExpiredDate = user.DriverLicenseExpiredDate;
        }

        public static void MapVerification(this User dbUser, User user)
        {
            dbUser.IdentityNumber = user.IdentityNumber;
            dbUser.Address1 = user.Address1;
            dbUser.Address2 = user.Address2;
            dbUser.PostalCode = user.PostalCode;
            dbUser.UserIcFrontImageUrl = user.UserIcFrontImageUrl;
            dbUser.UserIcBackImageUrl = user.UserIcBackImageUrl;
            dbUser.UserSelfieImageUrl = user.UserSelfieImageUrl;
            dbUser.State = user.State;
            dbUser.Status = 1; //Updated
        }
        public static void MapStatus(this User dbUser,int status)
        {
            dbUser.Status = status; 
        }
    }
}
