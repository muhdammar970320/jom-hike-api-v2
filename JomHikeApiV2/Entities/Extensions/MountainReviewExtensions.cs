﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.Entities.Extensions
{
    public static class MountainReviewExtensions
    {
        public static void Map(this MountainReview dbMountainReview, MountainReview mountainReview)
        {
            dbMountainReview.MountainDifficulty = mountainReview.MountainDifficulty;
            dbMountainReview.MountainRating = dbMountainReview.MountainRating;
            dbMountainReview.ReviewDescription = mountainReview.ReviewDescription;

        }
    }
}
