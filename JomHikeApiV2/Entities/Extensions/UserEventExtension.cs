using JomHikeApiV2.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.Entities.Extensions
{
    public static class UserEventExtension
    {
        public static void MapStatus(this UserEvent dbEvent, UserEvent events,string parameter)
        {
           
            dbEvent.Status =parameter;

            if (parameter.Equals("Cancel"))
                dbEvent.MaxAttempt = dbEvent.MaxAttempt + 1;

        }
    }
}
