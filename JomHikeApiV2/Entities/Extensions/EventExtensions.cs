﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.Entities.Extensions
{
    public static class EventExtensions
    {
        public static void Map(this Event dbEvent, Event events)
        {
            dbEvent.Name = events.Name;
            dbEvent.Location = events.Location;
            dbEvent.State = events.State;
            dbEvent.OrganizerName = events.OrganizerName;
            dbEvent.OrganizerRegistrationNumber = events.OrganizerRegistrationNumber;
            dbEvent.Description = events.Description;
            dbEvent.StartDate = events.StartDate;
            dbEvent.EndDate = events.EndDate;
            dbEvent.MountainId = events.MountainId;


        }
        public static void MapUpdateStatus(this Event dbEvent,string status)
        {
            dbEvent.Status = status;

        }
    }
}
