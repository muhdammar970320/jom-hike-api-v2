﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.Entities.Extensions
{
    public static class MountainExtensions
    {
        public static void Map(this Mountain dbMountain, Mountain mountain)
        {
            dbMountain.Name = mountain.Name;
            dbMountain.HeightFeet =mountain.HeightFeet;
            dbMountain.HeightMeter = mountain.HeightMeter;
            dbMountain.State = mountain.State;
            dbMountain.DestinationLatitude = mountain.DestinationLatitude;
            dbMountain.DestinationLongitude = mountain.DestinationLongitude;

        }
        public static void MapUpdateStatus(this Mountain dbMountain,string status)
        {
            dbMountain.Status = status;

        }

    }
}
