﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.Entities.Extensions
{
    public static class VehicleExtensions
    {
        public static void Map(this Vehicle dbVehicle, Vehicle vehicle)
        {
            dbVehicle.Name = vehicle.Name;
            dbVehicle.Brand = vehicle.Brand;
            dbVehicle.MakeYear = vehicle.MakeYear;
            dbVehicle.PlateNumber = vehicle.PlateNumber;
            dbVehicle.Color = vehicle.Color;
        }
    }
}
