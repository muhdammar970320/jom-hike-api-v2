﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.Entities.Utils
{
    public interface IImageWriter
    {
        Task<string> UploadImage(IFormFile file);
    }
}
