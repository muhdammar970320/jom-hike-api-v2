using JomHikeApiV2.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.Contracts
{
    public interface IUserEventRepository : IRepositoryBase<UserEvent>
    {
        //IEnumerable<EventExtended> GetAllEvents();
        UserEvent GetUserEventById(Guid userId);
        void CreateUserEvent(UserEvent userEvent);
        void UpdateUserEvent(UserEvent dbEvent, UserEvent events);
        void DeleteUserEvent(UserEvent userEvents);
        void UpdateUserEventStatus(UserEvent dbEvent, UserEvent events, string parameter);
    }
}
