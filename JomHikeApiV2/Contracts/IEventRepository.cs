﻿using Entities.Models;
using JomHikeApiV2.Entities.ExtendedModels;
using JomHikeApiV2.Entities.Models;
using JomHikeApiV2.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JomHikeApiV2.Entities.ExtendedModels.User;

namespace JomHikeApiV2.Contracts
{
    public interface IEventRepository
    {

        IEnumerable<UserJoinedEventExtended> GetUserJoinByEventId(int eventId);
        IEnumerable<EventExtended> GetAllEventsWeb();
        IEnumerable<EventExtended> GetAllEvents();
        Event GetEventById(int eventId);
        UserEvent GetUserEventById(Guid userId, int eventId);
        int GetEventIdByRideId(int rideId);
        void CreateEvent(Event events);
        void UpdateEvent(Event dbEvent, Event events);
        void DeleteEvent(Event events);
        void UpdateStatus(Event dbEvent,string status);
        int CountEventApproved();
        int CountEventRejected();
        int CountEventPending();
    }
}
