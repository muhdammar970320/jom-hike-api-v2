﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.Contracts
{
    public interface IMountainReviewRepository
    {
        IEnumerable<MountainReview> GetAllMountainReviews();
        MountainReview GetMountainReviewById(int mountainReviewId);
        void CreateMountainReview(MountainReview mountainReview);
        void UpdateMountainReview(MountainReview dbMountainReview, MountainReview mountainReview);
        void DeleteMountainReview(MountainReview mountainReview);
        bool IsReviewExists(Guid userId, int mountainId);
    }
}

