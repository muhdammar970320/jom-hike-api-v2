﻿using Entities.Models;
using JomHikeApiV2.Entities.ExtendedModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.Contracts
{
    public interface IMountainRepository
    {
        IEnumerable<Mountain> GetAllMountains();
        Mountain GetMountainById(int mountainId);
        void CreateMountain(Mountain mountain);
        void UpdateMountain(Mountain dbMountain, Mountain mountain);
        void DeleteMountain(Mountain mountain);
        void UpdateStatus(Mountain dbMountain, string status);
        IEnumerable<Mountain> GetAllMountainsWeb();
        IEnumerable<MountainReviewExtended> GetMountainReviewByMountainId(int mountainId);
        void UpdateMountainRatingAndDifficulty(int mountainId, double difficult, double rating);

        int CountMountainApproved();
        int CountMountainRejected();
        int CountMountainPending();

    }
}
