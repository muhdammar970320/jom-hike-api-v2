﻿using Entities.Models;
using JomHikeApiV2.Entities.ExtendedModels.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.Contracts
{
    public interface IRequestRepository
    {
        IEnumerable<RequestWithRideExtended> GetRequestByEventIdAndUserId(int eventId, Guid userId);
        IEnumerable<Request> GetAllRequests();
        Request GetRequestById(int requestId);
        void CreateRequest(Request request);
        void UpdateRequest(Request dbRequest, Request request);
        void DeleteRequest(Request request);
        bool IsRequestExists(Guid userId, int rideId);
        void ApproveRequest(Request dbRequest, Request request);
        void CancelRequest(Request dbRequest, Request request);
        void UpdatePaymentRequest(Request dbRequest, Request request);
    }
}
