﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace JomHikeApiV2.Contracts
{
    public interface IRepositoryBase<T>
    {
        //Basic Method that can implement to Entity repository
        //Customize method on Entity interface ex: IUserRepository
        IQueryable<T> FindAll();
        IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression);
        void Create(T entity);
        void Update(T entity);
        void Delete(T entity);
    }
}
