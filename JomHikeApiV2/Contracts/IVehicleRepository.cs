﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.Contracts
{
    public interface IVehicleRepository : IRepositoryBase<Vehicle>
    {
        IEnumerable<Vehicle> GetAllVehicles();
        Vehicle GetVehicleById(int vehicleId);
        void CreateVehicle(Vehicle vehicle);
        void UpdateVehicle(Vehicle dbVehicle, Vehicle vehicle);
        void DeleteVehicle(Vehicle vehicle);
        bool isVehicleExistInRide(int vehicleId);
    }
}
