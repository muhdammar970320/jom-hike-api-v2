﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.Contracts
{
    public interface IRepositoryWrapper
    {
        //Wrap Entity repo to register in Startup.cs
        IUserRepository User { get; }
        IVehicleRepository Vehicle { get; }
        IRequestRepository Request { get; }
        IMountainRepository Mountain { get; }
        IRideRepository Ride { get; }
        IEventRepository Event { get; }
        IMountainReviewRepository MountainReview { get; }
        IUserEventRepository UserEvent { get; }
        void Save();
    }
}
