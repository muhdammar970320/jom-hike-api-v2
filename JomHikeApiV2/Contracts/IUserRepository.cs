﻿using Entities.Models;
using JomHikeApiV2.Entities;
using JomHikeApiV2.Entities.ExtendedModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.Contracts
{
    public interface IUserRepository : IRepositoryBase<User>
    {
        //If want to implement specific method,put here.
        User GetUser(string userName);
        User GetUserById(Guid userId);
        bool IsUserExists(string userName);
        bool IsJoinEvent(Guid userId, int eventId);
        IEnumerable<User> GetAllUsers();
        void DeleteUser(User user);
        void UpdateUser(User dbUser, User user);
        void UpdateStatus(User dbUser,int status);
        void UpdateUserVerification(User dbUser, User user);
        UserExtended GetUserWithVehicles(Guid userId);
        UserExtended GetUserWithMountainReviews(Guid userId);
        UserExtended GetUserWithRequest(Guid userId); 
        IEnumerable<User> GetUserWitRides(Guid userId); 
        IEnumerable<Event> GetIncomingEventUserById(Guid userId);
        IEnumerable<Event> GetPreviousEventUserById(Guid userId);
        IEnumerable<Event> GetOrganizedEventUserById(Guid userId);
        UserExtended GetUserByIdWithAllDetails(Guid userId);

    }
}
