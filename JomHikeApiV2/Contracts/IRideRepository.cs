﻿using Entities.Models;
using JomHikeApiV2.Entities;
using JomHikeApiV2.Entities.ExtendedModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.Contracts
{
    public interface IRideRepository : IRepositoryBase<Ride>
    {
        IEnumerable<Ride> GetAllRides();
        IEnumerable<RideExtended> GetAllRidesByEventId(int eventId);
        bool IsRideExists(int vehicleId, int eventId);
        Ride GetRideById(int rideId);
        IEnumerable<Ride> GetRideByUserId(Guid userId);
        RequestRideExtended GetRideWithRequest(Guid userId, int eventId);
        int CountRideRequestByUserId(Guid userId, int eventId);
        void CreateRide(Ride ride);
        void UpdateRide(Ride dbRide, Ride ride);
        void ConfirmRide(int id, RequestRideExtended rideModel);
        void FinishRide(int id, RequestRideExtended rideModel);
        void DeleteRide(Ride ride);
        RequestRideExtended GetRideWithRequestByRideId(int rideId);
        
    }
}
