﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace JomHikeApiV2.Migrations
{
    public partial class InitialDatabaseCreation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Mountain",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(maxLength: 30, nullable: false),
                    HeightFeet = table.Column<float>(nullable: false),
                    HeightMeter = table.Column<float>(nullable: false),
                    State = table.Column<string>(nullable: false),
                    AverageDifficulty = table.Column<double>(nullable: false),
                    AverageRating = table.Column<double>(nullable: false),
                    DestinationLongitude = table.Column<double>(nullable: false),
                    DestinationLatitude = table.Column<double>(nullable: false),
                    Status = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Mountain", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UserName = table.Column<string>(maxLength: 15, nullable: false),
                    Password = table.Column<string>(maxLength: 250, nullable: false),
                    Salt = table.Column<string>(maxLength: 250, nullable: true),
                    FirstName = table.Column<string>(maxLength: 50, nullable: false),
                    LastName = table.Column<string>(maxLength: 50, nullable: false),
                    IdentityNumber = table.Column<string>(nullable: true),
                    ProfileImageUrl = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    Address1 = table.Column<string>(nullable: true),
                    Address2 = table.Column<string>(nullable: true),
                    PostalCode = table.Column<string>(nullable: true),
                    Role = table.Column<string>(nullable: true),
                    State = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    DriverLicenseNumber = table.Column<string>(nullable: true),
                    DriverLicenseExpiredDate = table.Column<DateTime>(nullable: false),
                    UserIcFrontImageUrl = table.Column<string>(nullable: true),
                    UserIcBackImageUrl = table.Column<string>(nullable: true),
                    UserSelfieImageUrl = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Event",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    Location = table.Column<string>(nullable: false),
                    State = table.Column<string>(maxLength: 20, nullable: false),
                    OrganizerName = table.Column<string>(maxLength: 50, nullable: false),
                    OrganizerRegistrationNumber = table.Column<string>(maxLength: 10, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EventImageUrl = table.Column<string>(nullable: true),
                    EndDate = table.Column<DateTime>(nullable: false),
                    Status = table.Column<string>(nullable: true),
                    UserId = table.Column<Guid>(nullable: false),
                    MountainId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Event", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Event_Mountain_MountainId",
                        column: x => x.MountainId,
                        principalTable: "Mountain",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Event_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MountainReview",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    ReviewDescription = table.Column<string>(nullable: false),
                    ReviewDate = table.Column<DateTime>(nullable: false),
                    MountainDifficulty = table.Column<double>(nullable: false),
                    MountainRating = table.Column<double>(nullable: false),
                    MountainId = table.Column<int>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MountainReview", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MountainReview_Mountain_MountainId",
                        column: x => x.MountainId,
                        principalTable: "Mountain",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MountainReview_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Vehicles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: false),
                    Brand = table.Column<string>(nullable: true),
                    MakeYear = table.Column<int>(nullable: true),
                    Color = table.Column<string>(nullable: true),
                    PlateNumber = table.Column<string>(nullable: true),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vehicles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Vehicles_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserEvent",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    EventId = table.Column<int>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    IsRequestRide = table.Column<int>(nullable: false),
                    IsOfferRide = table.Column<int>(nullable: false),
                    MaxAttempt = table.Column<int>(nullable: false),
                    Status = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserEvent", x => new { x.UserId, x.EventId });
                    table.UniqueConstraint("AK_UserEvent_Id", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserEvent_Event_EventId",
                        column: x => x.EventId,
                        principalTable: "Event",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserEvent_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Ride",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    RideStartTime = table.Column<DateTime>(nullable: false),
                    DestinationLongitude = table.Column<double>(nullable: false),
                    DestinationLatitude = table.Column<double>(nullable: false),
                    SeatAvailable = table.Column<int>(nullable: false),
                    TotalCost = table.Column<double>(nullable: false),
                    TotalDistance = table.Column<double>(nullable: false),
                    FinalTotalCost = table.Column<double>(nullable: false),
                    Status = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    VehicleId = table.Column<int>(nullable: false),
                    EventId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ride", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Ride_Event_EventId",
                        column: x => x.EventId,
                        principalTable: "Event",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Ride_Vehicles_VehicleId",
                        column: x => x.VehicleId,
                        principalTable: "Vehicles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Request",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    PickupLongitude = table.Column<double>(nullable: false),
                    PickupLatitude = table.Column<double>(nullable: false),
                    EstimatedCost = table.Column<double>(nullable: true),
                    TotalDistance = table.Column<double>(nullable: true),
                    FinalTotalCost = table.Column<double>(nullable: true),
                    FinalTotalDistance = table.Column<double>(nullable: true),
                    Rating = table.Column<double>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    Status = table.Column<string>(nullable: true),
                    PaymentStatus = table.Column<string>(nullable: true),
                    PaymentMethod = table.Column<int>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    RideId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Request", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Request_Ride_RideId",
                        column: x => x.RideId,
                        principalTable: "Ride",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Request_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Mountain",
                columns: new[] { "Id", "AverageDifficulty", "AverageRating", "DestinationLatitude", "DestinationLongitude", "HeightFeet", "HeightMeter", "Name", "State", "Status" },
                values: new object[,]
                {
                    { -1, 0.0, 0.0, 2.6985999999999999, 102.04810000000001, 2702f, 824f, "Gunung Angsi", "Negeri Sembilan", "Pending" },
                    { -2, 0.0, 0.0, 2.5434999999999999, 102.1691, 2900f, 884f, "Gunung Datuk", "Negeri Sembilan", "Pending" },
                    { -3, 0.0, 0.0, 4.6500000000000004, 101.3617, 7162f, 2181f, "Gunung Yong Belar", "Perak", "Pending" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Address1", "Address2", "DriverLicenseExpiredDate", "DriverLicenseNumber", "Email", "FirstName", "IdentityNumber", "LastName", "Password", "PhoneNumber", "PostalCode", "ProfileImageUrl", "Role", "Salt", "State", "Status", "UserIcBackImageUrl", "UserIcFrontImageUrl", "UserName", "UserSelfieImageUrl" },
                values: new object[,]
                {
                    { new Guid("460ed2fc-04b0-4431-ad50-e92b68a7d053"), "No 1, Jalan 2", "Klang, Selangor", new DateTime(2021, 1, 20, 1, 48, 46, 164, DateTimeKind.Local).AddTicks(7667), "123456789", "zkop78@gmail.com", "Muhammad Ammar", "970320-10-9999", "Abdul Aziz", "8WA2wo2geJzm8G9fI9J9Xty9P28wI54HUqiowBKiRKg=", "0172605960", "42100", "default.jpg", "User", "TTl6wU1baY8YChUce4diVQ==", "N9", 1, null, null, "ammar", null },
                    { new Guid("742d5cae-6184-4a32-a58d-36e23febf1a3"), "No 1, Jalan 2", "Klang, Selangor", new DateTime(2021, 1, 20, 1, 48, 46, 165, DateTimeKind.Local).AddTicks(7455), "123456789", "amanmos97@gmail.com", "Ahmad Hazman", "970320-10-9998", "Khairulanuar", "8WA2wo2geJzm8G9fI9J9Xty9P28wI54HUqiowBKiRKg=", "0172605960", "42100", "default.jpg", "User", "TTl6wU1baY8YChUce4diVQ==", "N9", 1, null, null, "hazman", null },
                    { new Guid("d923e0af-c142-4ce2-bdfa-142e6d0c6cf5"), "No 1, Jalan 2", "Klang, Selangor", new DateTime(2021, 1, 20, 1, 48, 46, 165, DateTimeKind.Local).AddTicks(7580), "123456789", "nik97@gmail.com", "Nik Farhan", "970320-10-9997", "Abdullah", "8WA2wo2geJzm8G9fI9J9Xty9P28wI54HUqiowBKiRKg=", "0172605960", "41300", "default.jpg", "User", "TTl6wU1baY8YChUce4diVQ==", "N9", 0, null, null, "nik", null }
                });

            migrationBuilder.InsertData(
                table: "Event",
                columns: new[] { "Id", "Description", "EndDate", "EventImageUrl", "Location", "MountainId", "Name", "OrganizerName", "OrganizerRegistrationNumber", "StartDate", "State", "Status", "UserId" },
                values: new object[,]
                {
                    { -1, "Hiking Gunung Angsi", new DateTime(2020, 2, 21, 1, 48, 46, 175, DateTimeKind.Local).AddTicks(7444), "default.jpg", "Bukit Putus", -1, "XPDC ke Gunung Angsi", "UiTM Seremban", "ABC123", new DateTime(2020, 2, 20, 1, 48, 46, 175, DateTimeKind.Local).AddTicks(7434), "Negeri Sembilan", "Pending", new Guid("742d5cae-6184-4a32-a58d-36e23febf1a3") },
                    { -2, "Hiking Gunung Datuk", new DateTime(2020, 3, 21, 1, 48, 46, 175, DateTimeKind.Local).AddTicks(9560), "default.jpg", "Rembau", -1, "XPDC ke Gunung Datuk", "USIM", "ABC1232", new DateTime(2020, 3, 20, 1, 48, 46, 175, DateTimeKind.Local).AddTicks(9555), "Negeri Sembilan", "Pending", new Guid("d923e0af-c142-4ce2-bdfa-142e6d0c6cf5") }
                });

            migrationBuilder.InsertData(
                table: "MountainReview",
                columns: new[] { "Id", "MountainDifficulty", "MountainId", "MountainRating", "ReviewDate", "ReviewDescription", "UserId" },
                values: new object[,]
                {
                    { -3, 4.0, -2, 1.0, new DateTime(2020, 1, 20, 1, 48, 46, 174, DateTimeKind.Local).AddTicks(4872), "Too easy, for professional", new Guid("460ed2fc-04b0-4431-ad50-e92b68a7d053") },
                    { -2, 4.0, -1, 4.0, new DateTime(2020, 1, 20, 1, 48, 46, 174, DateTimeKind.Local).AddTicks(4840), "Challenging, Suitable for training", new Guid("742d5cae-6184-4a32-a58d-36e23febf1a3") },
                    { -1, 2.0, -1, 5.0, new DateTime(2020, 1, 20, 1, 48, 46, 174, DateTimeKind.Local).AddTicks(3094), "Nice View, Suitable for beginner", new Guid("d923e0af-c142-4ce2-bdfa-142e6d0c6cf5") },
                    { -4, 5.0, -3, 2.0, new DateTime(2020, 1, 20, 1, 48, 46, 174, DateTimeKind.Local).AddTicks(4882), "Challenging, Not Suitable for training", new Guid("d923e0af-c142-4ce2-bdfa-142e6d0c6cf5") }
                });

            migrationBuilder.InsertData(
                table: "Vehicles",
                columns: new[] { "Id", "Brand", "Color", "MakeYear", "Name", "PlateNumber", "UserId" },
                values: new object[,]
                {
                    { -1, "Perodua", "Yello", 2017, "Axia", "ABC125", new Guid("742d5cae-6184-4a32-a58d-36e23febf1a3") },
                    { -2, "Produa", "Red", 2017, "Viva", "ABC123", new Guid("742d5cae-6184-4a32-a58d-36e23febf1a3") },
                    { -3, "Perodua", "Red", 2014, "Myvi", "ABC124", new Guid("d923e0af-c142-4ce2-bdfa-142e6d0c6cf5") }
                });

            migrationBuilder.InsertData(
                table: "Ride",
                columns: new[] { "Id", "CreatedAt", "DestinationLatitude", "DestinationLongitude", "EventId", "FinalTotalCost", "RideStartTime", "SeatAvailable", "Status", "TotalCost", "TotalDistance", "VehicleId" },
                values: new object[,]
                {
                    { -1, new DateTime(2020, 1, 20, 1, 48, 46, 177, DateTimeKind.Local).AddTicks(1842), 2.65341, 101.90673, -1, 50.5, new DateTime(2020, 1, 29, 10, 48, 46, 177, DateTimeKind.Local).AddTicks(1838), 4, "Ongoing", 100.0, 50.5, -1 },
                    { -2, new DateTime(2020, 1, 20, 1, 48, 46, 177, DateTimeKind.Local).AddTicks(3713), 2.65299, 101.91927, -2, 50.5, new DateTime(2020, 2, 7, 23, 48, 46, 177, DateTimeKind.Local).AddTicks(3708), 4, "Ongoing", 80.0, 67.5, -2 },
                    { -3, new DateTime(2020, 1, 20, 1, 48, 46, 177, DateTimeKind.Local).AddTicks(3730), 4.4721200000000003, 101.38014200000001, -2, 50.5, new DateTime(2020, 1, 25, 6, 48, 46, 177, DateTimeKind.Local).AddTicks(3728), 4, "Ongoing", 70.0, 67.5, -3 }
                });

            migrationBuilder.InsertData(
                table: "UserEvent",
                columns: new[] { "UserId", "EventId", "Id", "IsOfferRide", "IsRequestRide", "MaxAttempt", "Status" },
                values: new object[,]
                {
                    { new Guid("d923e0af-c142-4ce2-bdfa-142e6d0c6cf5"), -1, -1, 0, 0, 0, "Joined" },
                    { new Guid("d923e0af-c142-4ce2-bdfa-142e6d0c6cf5"), -2, -2, 0, 0, 0, "Joined" },
                    { new Guid("742d5cae-6184-4a32-a58d-36e23febf1a3"), -2, -3, 0, 0, 0, "Joined" }
                });

            migrationBuilder.InsertData(
                table: "Request",
                columns: new[] { "Id", "CreatedAt", "EstimatedCost", "FinalTotalCost", "FinalTotalDistance", "PaymentMethod", "PaymentStatus", "PickupLatitude", "PickupLongitude", "Rating", "RideId", "Status", "TotalDistance", "UserId" },
                values: new object[,]
                {
                    { -1, new DateTime(2020, 1, 20, 1, 48, 46, 178, DateTimeKind.Local).AddTicks(5340), 32.0, null, null, 0, "Not Paid Yet", 3.0741499999999999, 101.51857800000001, null, -1, "Pending", null, new Guid("d923e0af-c142-4ce2-bdfa-142e6d0c6cf5") },
                    { -2, new DateTime(2020, 1, 20, 1, 48, 46, 178, DateTimeKind.Local).AddTicks(7218), 40.200000000000003, null, null, 0, "Not Paid Yet", 3.0741499999999999, 101.51857800000001, null, -2, "Pending", null, new Guid("d923e0af-c142-4ce2-bdfa-142e6d0c6cf5") },
                    { -3, new DateTime(2020, 1, 20, 1, 48, 46, 178, DateTimeKind.Local).AddTicks(7251), 40.200000000000003, null, null, 0, "Not Paid Yet", 2.8887999999999998, 101.775192, null, -3, "Pending", null, new Guid("742d5cae-6184-4a32-a58d-36e23febf1a3") }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Event_MountainId",
                table: "Event",
                column: "MountainId");

            migrationBuilder.CreateIndex(
                name: "IX_Event_UserId",
                table: "Event",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_MountainReview_MountainId",
                table: "MountainReview",
                column: "MountainId");

            migrationBuilder.CreateIndex(
                name: "IX_MountainReview_UserId",
                table: "MountainReview",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Request_RideId",
                table: "Request",
                column: "RideId");

            migrationBuilder.CreateIndex(
                name: "IX_Request_UserId",
                table: "Request",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Ride_EventId",
                table: "Ride",
                column: "EventId");

            migrationBuilder.CreateIndex(
                name: "IX_Ride_VehicleId",
                table: "Ride",
                column: "VehicleId");

            migrationBuilder.CreateIndex(
                name: "IX_UserEvent_EventId",
                table: "UserEvent",
                column: "EventId");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicles_UserId",
                table: "Vehicles",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MountainReview");

            migrationBuilder.DropTable(
                name: "Request");

            migrationBuilder.DropTable(
                name: "UserEvent");

            migrationBuilder.DropTable(
                name: "Ride");

            migrationBuilder.DropTable(
                name: "Event");

            migrationBuilder.DropTable(
                name: "Vehicles");

            migrationBuilder.DropTable(
                name: "Mountain");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
