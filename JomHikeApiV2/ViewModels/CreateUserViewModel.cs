﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.ViewModels
{
    public class CreateUserViewModel
    {

        [Required(ErrorMessage = "Name is required")]
        [Display(Name = "Username", Prompt = "Eg: ammar97")]
        [RegularExpression("^(?=.{6,8}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$",
            ErrorMessage = "The {0} does not meet requirements." +
            " Must have number and character in between 6-8 characters only")]
        [MaxLength(8, ErrorMessage = "Username exceed 8 characters")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password",
            ErrorMessage = "Password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "First Name exceed 30 characters")]
        [Display(Name = "First Name", Prompt = "Muhammad Ammar")]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "Last Name exceed 30 characters")]
        [Display(Name = "Last Name", Prompt = "Eg: Abdul Aziz")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "No IC", Prompt = "Eg: 970320-10-7777")]
        [RegularExpression(@"^\d{6}\-\d{2}\-\d{4}$", ErrorMessage = "The {0} does not meet requirements. Ex: 970320-10-7777")]
        public string IdentityNumber { get; set; }

        [Required]
        [Display(Name = "Phone Number", Prompt = "Eg: 0126585258")]
        [RegularExpression("^(\\+?6?01)[0-46-9]-*[0-9]{7,8}$", ErrorMessage = "The {0} does not meet requirements.")]
        public string PhoneNumber { get; set; }

        [Required]
        [Display(Name = "Address 1", Prompt = "Eg: No 2, Jalan Kg Subang")]
        public string Address1 { get; set; }

        [Required]
        [Display(Name = "Address 2", Prompt = "Eg: Shah Alam, Selangor")]
        public string Address2 { get; set; }

        [Required]
        [Display(Name = "Postal Code", Prompt = "Eg: 40150")]
        [MinLength(5, ErrorMessage = "Postal Code minimum 5 characters")]
        [MaxLength(5, ErrorMessage = "Postal Code exceed 5 characters")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "The {0} does not meet requirements. Ex: 40150")]
        public string PostalCode { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email", Prompt = "Eg: muhdammar970320@gmail.com")]
        public string Email { get; set; }

        [Required]
        public string Role { get; set; }

    }
}
