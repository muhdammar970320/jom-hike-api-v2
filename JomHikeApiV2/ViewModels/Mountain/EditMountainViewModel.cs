﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.ViewModels.Mountain
{
    public class EditMountainViewModel : CreateMountainViewModel
    {
        public Double AverageDifficulty { get; set; }
        public Double AverageRating { get; set; }
    }
}
