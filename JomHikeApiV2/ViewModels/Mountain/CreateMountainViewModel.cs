﻿using Entities.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.ViewModels.Mountain
{
    public class CreateMountainViewModel : IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Name", Prompt = "Eg: Gunung Kinabalu")]
        [MaxLength(30, ErrorMessage = "Mountain Name exceeds 30 characters")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Height in Feet", Prompt = "Eg: 5323")]
        public float HeightFeet { get; set; }

        [Required]
        [Display(Name = "Height in Meter", Prompt = "Eg: 2321")]
        public float HeightMeter { get; set; }

        [Required]
        public string State { get; set; }

        [Required]
        [Display(Name = "Longitude", Prompt = "Eg: 23.421")]
        public Double DestinationLongitude { get; set; }
        [Required]
        [Display(Name = "Latitude", Prompt = "Eg: 123.34")]
        public Double DestinationLatitude { get; set; }
    }
}
