﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.ViewModels
{
    public class DashboardViewModel
    {
        public int events_count { get; set; }
        public int mountain_reviews_count { get; set; }
        public int vehicles_count { get; set; }
        public int mountains_count { get; set; }
        public int users_count { get; set; }
        public int rides_count { get; set; }
        public int requests_count { get; set; }

        public int mountain_pending_count { get; set; }
        public int mountain_approved_count { get; set; }
        public int mountain_rejected_count { get; set; }

        public int events_approved_count { get; set; }
        public int events_pending_count { get; set; }
        public int events_rejected_count { get; set; }

    }
}
