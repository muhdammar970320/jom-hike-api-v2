﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Entities;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace JomHikeApiV2
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().MigrateDatabase().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                //.UseUrls("http://*:5000", "http://192.168.0.185:5000")
                .UseUrls("http://*:5000", "http://192.168.43.144:5000")
                //.UseUrls("http://192.168.43.144:5000")
                //.UseUrls("http://*:5000")
                .UseStartup<Startup>();
                //.UseKestrel(options =>
                //{
                //    options.ListenAnyIP(Int32.Parse(System.Environment.GetEnvironmentVariable("PORT")));
                //});
    }
}
