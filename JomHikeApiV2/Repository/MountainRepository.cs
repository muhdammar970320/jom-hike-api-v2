﻿
using Entities;
using Entities.Models;
using JomHikeApiV2.Contracts;
using JomHikeApiV2.Entities;
using JomHikeApiV2.Entities.ExtendedModels;
using JomHikeApiV2.Entities.Extensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repository
{
    public class MountainRepository : RepositoryBase<Mountain>, IMountainRepository
    {
        public MountainRepository(RepositoryContext repositoryContext) : base(repositoryContext)
        {
        }

        public void CreateMountain(Mountain mountain)
        {
            Create(mountain);
        }
        public void DeleteMountain(Mountain mountain)
        {
            Delete(mountain);
        }
        public IEnumerable<Mountain> GetAllMountainsWeb()
        {
            return FindAll().OrderBy(m => m.Name).ToList();
        }
        public IEnumerable<Mountain> GetAllMountains()
        {
            return FindAll().OrderBy(m => m.Name).Where(m=>m.Status.Equals("Approved")).ToList();
        }
        public int CountMountainApproved()
        {
            return FindAll().OrderBy(m => m.Name).Where(m => m.Status.Equals("Approved")).Count();
        }
        public int CountMountainPending()
        {
            return FindAll().OrderBy(m => m.Name).Where(m => m.Status.Equals("Pending")).Count();
        }
        public int CountMountainRejected()
        {
            return FindAll().OrderBy(m => m.Name).Where(m => m.Status.Equals("Rejected")).Count();
        }

        public Mountain GetMountainById(int mountainId)
        {
            return FindByCondition(mountain => mountain.Id.Equals(mountainId)).Include(mountain => mountain.MountainReviews).ToList().FirstOrDefault();
        }

        public void UpdateMountain(Mountain dbMountain, Mountain mountain)
        {
            dbMountain.Map(mountain);
            Update(dbMountain);
        }
        public void UpdateMountainRatingAndDifficulty(int mountainId , double difficult, double rating)
        {
            var mountainReviews = GetMountainReviewByMountainId(mountainId);
            double averageDifficulty = mountainReviews.Sum(mr => mr.MountainDifficulty);
            double averageRating = mountainReviews.Sum(mr => mr.MountainRating);
            int count = mountainReviews.Count();
            if (count == 0)
            {
                averageDifficulty = difficult;
                averageRating = rating;

            }
            else
            {
                //Count + 1 because current count is not counted with the latest data from request since
                // it not save yet, so the latest data does not enter the database yet.
                averageDifficulty = ((averageDifficulty + difficult) / (count+1));
                averageRating = ((averageRating + rating)/ (count+1));
            }


            var mountain = RepositoryContext.Mountains.Where(m => m.Id.Equals(mountainId)).FirstOrDefault();
            mountain.AverageDifficulty = Math.Round(averageDifficulty,2);
            mountain.AverageRating = Math.Round(averageRating, 2);
            Update(mountain);
        }
        public void UpdateStatus(Mountain dbMountain,string status)
        {
            dbMountain.MapUpdateStatus(status);
            Update(dbMountain);
        }

        public IEnumerable<MountainReviewExtended> GetMountainReviewByMountainId(int mountainId)
        {
            return RepositoryContext.MountainReviews.Where(mr => mr.MountainId.Equals(mountainId)).Select(e => new MountainReviewExtended()
            {
                Id = e.Id,
                Mountain = e.Mountain,
                MountainDifficulty = e.MountainDifficulty,
                MountainRating = e.MountainRating,
                ReviewDate = e.ReviewDate,
                ReviewDescription = e.ReviewDescription,
                User = new UserMountainReviewExtended()
                {
                  UserName = e.User.UserName  
                }
            }).ToList();
        }
    }
}
