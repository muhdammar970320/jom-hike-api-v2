﻿
using Entities;
using Entities.Models;
using JomHikeApiV2.Contracts;
using JomHikeApiV2.Entities.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repository
{
    public class MountainReviewRepository : RepositoryBase<MountainReview>, IMountainReviewRepository
    {
        public MountainReviewRepository(RepositoryContext repositoryContext) : base(repositoryContext)
        {
        }

        public void CreateMountainReview(MountainReview mountainReview)
        {
            Create(mountainReview);
        }

        public void DeleteMountainReview(MountainReview mountainReview)
        {
            Delete(mountainReview);
        }

        public IEnumerable<MountainReview> GetAllMountainReviews()
        {
            return FindAll().OrderBy(mr => mr.Id).ToList();
        }

        public MountainReview GetMountainReviewById(int mountainReviewId)
        {
            return FindByCondition(mountainReview => mountainReview.Id.Equals(mountainReviewId))
               .FirstOrDefault();
        }

        public bool IsReviewExists(Guid userId, int mountainId)
        {
            return RepositoryContext.MountainReviews.Any(mr => mr.MountainId.Equals(mountainId)&& mr.UserId.Equals(userId));
        }

        public void UpdateMountainReview(MountainReview dbMountainReview, MountainReview mountainReview)
        {
            dbMountainReview.Map(mountainReview);
            Update(dbMountainReview);
        }
       
    }
}
