﻿
using Entities;
using JomHikeApiV2.Contracts;
using JomHikeApiV2.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private RepositoryContext _repoContext;
        private IUserRepository _user;
        private IVehicleRepository _vehicle;
        private IRequestRepository _request;
        private IMountainRepository _mountain;
        private IRideRepository _ride;
        private IEventRepository _event;
        private IMountainReviewRepository _mountainReview;
        private IUserEventRepository _userEvent;


        public IUserRepository User
        {
            get
            {
                if (_user == null)
                {
                    _user = new UserRepository(_repoContext);
                }

                return _user;
            }
        }

        public IVehicleRepository Vehicle
        {
            get
            {
                if (_vehicle == null)
                {
                    _vehicle = new VehicleRepository(_repoContext);
                }

                return _vehicle;
            }
        }
        public IRequestRepository Request
        {
            get
            {
                if (_request == null)
                {
                    _request = new RequestRepository(_repoContext);
                }

                return _request;
            }
        }
        public IMountainRepository Mountain
        {
            get
            {
                if (_mountain == null)
                {
                    _mountain = new MountainRepository(_repoContext);
                }

                return _mountain;
            }
        }
        public IRideRepository Ride
        {
            get
            {
                if (_ride == null)
                {
                    _ride = new RideRepository(_repoContext);
                }

                return _ride;
            }
        }
        public IEventRepository Event
        {
            get
            {
                if (_event == null)
                {
                    _event = new EventRepository(_repoContext);
                }

                return _event;
            }
        }

        public IMountainReviewRepository MountainReview
        {
            get
            {
                if (_mountainReview == null)
                {
                    _mountainReview = new MountainReviewRepository(_repoContext);
                }

                return _mountainReview;
            }
        }
        public IUserEventRepository UserEvent
        {
            get
            {
                if (_userEvent == null)
                {
                    _userEvent = new UserEventRepository(_repoContext);
                }

                return _userEvent;
            }
        }

        public RepositoryWrapper(RepositoryContext repositoryContext)
        {
            _repoContext = repositoryContext;
        }

        public void Save()
        {
            _repoContext.SaveChanges();
        }
    }
}
