﻿
using Entities;
using Entities.Extensions;
using Entities.Models;
using JomHikeApiV2.Contracts;
using JomHikeApiV2.Entities;
using JomHikeApiV2.Entities.ExtendedModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repository
{
    public class UserRepository : RepositoryBase<User>, IUserRepository
    {
        public UserRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }

        public void DeleteUser(User user)
        {
            Delete(user);
        }

        public IEnumerable<User> GetAllUsers()
        {
            return FindAll().OrderBy(u => u.FirstName).ToList();
        }

        public User GetUser(string userName)
        {
            return FindByCondition(user => user.UserName.Equals(userName)).FirstOrDefault();

        }

        public User GetUserById(Guid userId)
        {
            return FindByCondition(u => u.Id.Equals(userId))
            .FirstOrDefault();
        }
        public UserExtended GetUserByIdWithAllDetails(Guid userId)
        {
            var user = GetUserById(userId);
            return new UserExtended(user)
            {
                Vehicles = RepositoryContext.Vehicles.Where(mr => mr.UserId.Equals(userId)),
                MountainReviews = RepositoryContext.MountainReviews.Where(mr => mr.UserId.Equals(userId)),
                Events = RepositoryContext.Events.Where(mr => mr.UserId.Equals(userId)).Include(r => r.Mountain).ToList(),
                UserEvents = RepositoryContext.UserEvents.Where(mr => mr.UserId.Equals(userId)).Include(r => r.Event).ToList(),
                Requests = RepositoryContext.Requests.Where(mr => mr.UserId.Equals(userId)),
                Rides = RepositoryContext.Rides.Where(mr => mr.Vehicle.UserId.Equals(userId)).Include(r => r.Event).ToList(),
            };
        }

        public UserExtended GetUserWithVehicles(Guid userId)
        {
            return new UserExtended(GetUserById(userId))
            {
                Vehicles = RepositoryContext.Vehicles.Where(v => v.UserId.Equals(userId))
            };
        }

        public UserExtended GetUserWithMountainReviews(Guid userId)
        {
            return new UserExtended(GetUserById(userId))
            {
                MountainReviews = RepositoryContext.MountainReviews.Where(mr => mr.UserId.Equals(userId))
            };
        }

        public UserExtended GetUserWithRequest(Guid userId)
        {
            return new UserExtended(GetUserById(userId))
            {
                Requests = RepositoryContext.Requests.Where(r => r.UserId.Equals(userId))
            };
        }

        public IEnumerable<User> GetUserWitRides(Guid userId)
        {
                return RepositoryContext.Users.Where(u=>u.Id.Equals(userId)).Include(user => user.Vehicles).ThenInclude(vehicles => vehicles.Rides).ToList(); 
        }

        public bool IsJoinEvent(Guid userId, int eventId)
        {
            return RepositoryContext.UserEvents.Any(userEvent => userEvent.UserId.Equals(userId) && userEvent.EventId.Equals(eventId));
        }

        public bool IsUserExists(string userName)
        {

            return RepositoryContext.Users.Any(user => user.UserName.Equals(userName));
        }

        public void UpdateUser(User dbUser, User user)
        {
            dbUser.Map(user);
            Update(dbUser);
        }

        public void UpdateUserVerification(User dbUser, User user)
        {
            dbUser.MapVerification(user);
            Update(dbUser);
        }
        public void UpdateStatus(User dbUser, int status)
        {
            dbUser.MapStatus(status);
            Update(dbUser);
        }

        public IEnumerable<Event> GetIncomingEventUserById(Guid userId)
        {
            return RepositoryContext.UserEvents
                  .Where(mr => mr.UserId.Equals(userId))
                  .Select(r => r.Event)
                  .Where(r => r.EndDate >= DateTime.Now)
                  .Include(r => r.Mountain).ToList();
        }
        public IEnumerable<Event> GetPreviousEventUserById(Guid userId)
        {
            return RepositoryContext.UserEvents
                .Where(mr => mr.UserId.Equals(userId))
                .Select(r => r.Event)
                .Where(r=> r.EndDate <= DateTime.Now)
                .Include(r => r.Mountain).ToList();
        }
        public IEnumerable<Event> GetOrganizedEventUserById(Guid userId)
        {
            return RepositoryContext.Events.Where(mr => mr.UserId.Equals(userId)).Include(r => r.Mountain).ToList();
        }
    }
}
