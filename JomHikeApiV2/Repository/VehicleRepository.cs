﻿
using Entities;
using Entities.Models;
using JomHikeApiV2.Contracts;
using JomHikeApiV2.Entities.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repository
{
    public class VehicleRepository : RepositoryBase<Vehicle>, IVehicleRepository
    {
        public VehicleRepository(RepositoryContext repositoryContext) : base(repositoryContext)
        {
        }

        public void CreateVehicle(Vehicle vehicle)
        {
            Create(vehicle);
        }

        public void DeleteVehicle(Vehicle vehicle)
        {
            Delete(vehicle);
        }

        public IEnumerable<Vehicle> GetAllVehicles()
        {
            return FindAll().OrderBy(v => v.Name).ToList();
        }

        public Vehicle GetVehicleById(int vehicleId)
        {
            return FindByCondition(v => v.Id.Equals(vehicleId))
                .FirstOrDefault();
        }

        public bool isVehicleExistInRide(int vehicleId)
        {
            return RepositoryContext.Rides.Any(ride => ride.VehicleId.Equals(vehicleId));
        }

        public void UpdateVehicle(Vehicle dbVehicle, Vehicle vehicle)
        {
            dbVehicle.Map(vehicle);
            Update(dbVehicle);
        }
    }
}
