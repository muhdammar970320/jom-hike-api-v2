﻿
using Entities;
using Entities.Extensions;
using Entities.Models;
using JomHikeApiV2.Contracts;
using JomHikeApiV2.Entities;
using JomHikeApiV2.Entities.ExtendedModels;
using JomHikeApiV2.Entities.ExtendedModels.Event;
using JomHikeApiV2.Entities.ExtendedModels.User;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repository
{
    public class RideRepository : RepositoryBase<Ride>, IRideRepository
    {
        public RideRepository(RepositoryContext repositoryContext) : base(repositoryContext)
        {
        }

        public void CreateRide(Ride ride)
        {
            Create(ride);
        }

        public void DeleteRide(Ride ride)
        {
            Delete(ride);
        }

        public IEnumerable<Ride> GetAllRides()
        {
            return FindAll().OrderBy(r => r.Id).ToList();
        }

        public IEnumerable<RideExtended> GetAllRidesByEventId(int eventId)
        {
            return RepositoryContext.Rides.Where(r => r.EventId.Equals(eventId))
                .Select(r => new RideExtended()
                {
                    Id = r.Id,
                    RideStartTime = r.RideStartTime,
                    DestinationLongitude = r.DestinationLongitude,
                    DestinationLatitude = r.DestinationLatitude,
                    SeatAvailable = r.SeatAvailable,
                    TotalCost = r.TotalCost,
                    TotalDistance = r.TotalDistance,
                    Vehicle = RepositoryContext.Vehicles.Where(v => v.Id.Equals(r.VehicleId)).Select(v => new VehicleExtended()
                              {
                                  Id = v.Id,
                                  Name = v.Name,
                                  Brand = v.Name,
                                  MakeYear = v.MakeYear,
                                  Color = v.Color,
                                  PlateNumber = v.PlateNumber, 
                                  User = RepositoryContext.Users.Where(u => u.Id.Equals(v.UserId)).Select(u => new UserVehicleExtended()
                                  {
                                      Id = u.Id,
                                      FirstName = u.FirstName,
                                      LastName = u.LastName,
                                      IdentityNumber = u.IdentityNumber,
                                      PhoneNumber = u.PhoneNumber,
                                      Address1 = u.Address1,
                                      Address2 = u.Address2,
                                      PostalCode = u.PostalCode,
                                      Role = u.Role
                                   }).FirstOrDefault()
                          }).FirstOrDefault(),
                  Event = RepositoryContext.Events.Where(e => e.Id.Equals(r.EventId)).Select(e => new EventRideExtended()
                    {
                       Id = e.Id,
                       Name = e.Name,
                       Location = e.Location,
                       State = e.State,
                       Mountain = e.Mountain
                    }).FirstOrDefault(),
                 })
                 .ToList();
        }

        public Ride GetRideById(int rideId)
        {
            return FindByCondition(r => r.Id.Equals(rideId))
             .FirstOrDefault();
        }

        public bool IsRideExists(int vehicleId, int eventId )
        {
            //Vehicle vehicle = RepositoryContext.Vehicles.Where(v => v.Id.Equals(vehicleId)).FirstOrDefault();
            return RepositoryContext.Rides.Any(ride => ride.VehicleId.Equals(vehicleId) && ride.EventId.Equals(eventId));
        }

        public void UpdateRide(Ride dbRide, Ride ride)
        {
            dbRide.Map(ride);
            Update(dbRide);
        }
        public RequestRideExtended GetRideWithRequestByRideId(int rideId)
        {

            return RepositoryContext.Rides.Where(r => r.Id == rideId)
                    .Select(ride => new RequestRideExtended()
                    {
                        Id = ride.Id,
                        RideStartTime = ride.RideStartTime,
                        CreatedAt = ride.CreatedAt,
                        SeatAvailable = ride.SeatAvailable,
                        DestinationLatitude = ride.DestinationLatitude,
                        DestinationLongitude = ride.DestinationLongitude,
                        Status = ride.Status,
                        vehicle = RepositoryContext.Vehicles.Where(v => v.Id.Equals(ride.VehicleId)).Select(v => new VehicleExtended()
                        {
                            Id = v.Id,
                            Brand = v.Brand,
                            Color = v.Color,
                            MakeYear = v.MakeYear,
                            Name = v.Name,
                            PlateNumber = v.PlateNumber,
                            User =  RepositoryContext.Users.Where(u => u.Id.Equals(v.UserId)).Select(u => new UserVehicleExtended()
                            {
                                Id = u.Id,
                                FirstName = u.FirstName,
                                LastName = u.LastName,
                                IdentityNumber = u.IdentityNumber,
                                PhoneNumber = u.PhoneNumber,
                                Address1 = u.Address1,
                                Address2 = u.Address2,
                                PostalCode = u.PostalCode,
                                Role = u.Role
                            }).FirstOrDefault()
                        }).FirstOrDefault(),
                        TotalCost = ride.TotalCost,
                        TotalDistance = ride.TotalDistance,
                        FinalTotalCost = ride.FinalTotalCost,
                        Requests = RepositoryContext.Requests.Where(r => r.RideId.Equals(ride.Id)).Select(r => new RequestExtended()
                        {
                            Id = r.Id,
                            PickupLatitude = r.PickupLatitude,
                            PickupLongitude = r.PickupLongitude,
                            EstimatedCost = r.EstimatedCost,
                            FinalTotalCost = r.FinalTotalCost,
                            FinalTotalDistance = r.FinalTotalDistance,
                            Rating = r.Rating,
                            CreatedAt = r.CreatedAt,
                            Status = r.Status,
                            TotalDistance = r.TotalDistance,
                            User = RepositoryContext.Users.Where(u => u.Id.Equals(r.UserId)).Select(u => new UserRequestRideExtended()
                            {
                                Id = u.Id,
                                FirstName = u.FirstName,
                                LastName = u.LastName,
                            }).FirstOrDefault()

                        }).ToList(),
                        Mountain = RepositoryContext.Events.Where(e => e.Id.Equals(ride.EventId)).Select(e => new MountainExtended()
                        {
                            Id = e.MountainId,
                            Name = e.Mountain.Name,
                            DestinationLatitude = e.Mountain.DestinationLatitude,
                            DestinationLongitude = e.Mountain.DestinationLongitude
                        }).FirstOrDefault(),
                    })
                    .FirstOrDefault();
        }
        public RequestRideExtended GetRideWithRequest(Guid userId, int eventId)
        {
           
            return RepositoryContext.Rides.Where(r => r.EventId.Equals(eventId)&& r.Vehicle.UserId.Equals(userId))
                    .Select(ride => new RequestRideExtended()
                    {
                        Id = ride.Id,
                        SeatAvailable = ride.SeatAvailable,
                        DestinationLatitude = ride.DestinationLatitude,
                        DestinationLongitude = ride.DestinationLongitude,
                        FinalTotalCost = ride.FinalTotalCost,
                        Status =  ride.Status,
                        Requests = RepositoryContext.Requests.Where(r => r.RideId.Equals(ride.Id)).Select(r => new RequestExtended()
                        {
                            Id = r.Id,
                            PickupLatitude = r.PickupLatitude,
                            PickupLongitude = r.PickupLongitude,
                            EstimatedCost = r.EstimatedCost,
                            FinalTotalCost = r.FinalTotalCost,
                            Rating = r.Rating,
                            CreatedAt = r.CreatedAt,
                            Status = r.Status,
                            TotalDistance = r.TotalDistance,
                            User = RepositoryContext.Users.Where(u => u.Id.Equals(r.UserId)).Select(u => new UserRequestRideExtended()
                            {
                                FirstName = u.FirstName,
                                LastName = u.LastName,
                            }).FirstOrDefault()

                        }).ToList(),
                        Mountain = RepositoryContext.Events.Where(e => e.Id.Equals(ride.EventId)).Select(e => new MountainExtended()
                        {
                          Id = e.MountainId,
                          Name = e.Mountain.Name,
                          DestinationLatitude = e.Mountain.DestinationLatitude,
                          DestinationLongitude = e.Mountain.DestinationLongitude
                        }).FirstOrDefault(),
                    })
                    .FirstOrDefault();
        }
        public IEnumerable<Ride> GetRideByUserId(Guid userId)
        {

            return RepositoryContext.Rides.Where(r => r.Vehicle.UserId.Equals(userId))
                    .ToList();
        }

        public int CountRideRequestByUserId(Guid userId, int eventId)
        {
            //Count total request of rider to driver, status need to be "Approved"
            var ride = RepositoryContext.Rides.Where(r => r.EventId.Equals(eventId) && r.Vehicle.UserId.Equals(userId)).FirstOrDefault();

            //Check if ride is null
            if (ride == null)
            {
                return -1;
            }
            var id = ride.Id;
           
             return RepositoryContext.Requests.Where(r => r.RideId == id && r.Status.Equals("Approved")).Count();

        }

        public void ConfirmRide(int id, RequestRideExtended rideModel )
        {

            var existingRide = RepositoryContext.Rides
                                 .Where(p => p.Id == id)
                                 .Include(p => p.Requests).Where(r => r.Requests.Any( request => request.Status == "Approved"))
                                 .SingleOrDefault();
            if (existingRide != null)
            {
                // Update parent
                //RepositoryContext.Entry(existingRide).CurrentValues.SetValues(rideModel);
                existingRide.TotalCost = rideModel.TotalCost;
                existingRide.TotalDistance = rideModel.TotalDistance;
                existingRide.FinalTotalCost = rideModel.FinalTotalCost;
                // Set Ride status to confirm
                existingRide.Status = "Confirmed";

                // Set Request status to confirm
                foreach (var requestModel in rideModel.Requests.ToList())
                {
                    var existingRequest = existingRide.Requests.Where(r => r.Id == requestModel.Id).SingleOrDefault();
                    if (existingRequest != null)
                    {
                        //RepositoryContext.Entry(existingRequest).CurrentValues.SetValues(requestModel);
                        existingRequest.FinalTotalCost = requestModel.FinalTotalCost;
                        existingRequest.FinalTotalDistance = requestModel.FinalTotalDistance;
                        existingRequest.Status = "Confirmed";
                    }
                       
                }

            }
            RepositoryContext.SaveChanges();
        }

        public void FinishRide(int id, RequestRideExtended rideModel)
        {

            var existingRide = RepositoryContext.Rides
                                 .Where(p => p.Id == id)
                                 .Include(p => p.Requests).Where(r => r.Requests.Any(request => request.Status == "Confirmed"))
                                 .SingleOrDefault();
            if (existingRide != null)
            {
                // Update parent
                //RepositoryContext.Entry(existingRide).CurrentValues.SetValues(rideModel);
                // Set Ride status to confirm
                existingRide.Status = "Finished";

                // Set Request status to confirm
                foreach (var requestModel in rideModel.Requests.ToList())
                {
                    var existingRequest = existingRide.Requests.Where(r => r.Id == requestModel.Id).SingleOrDefault();
                    if (existingRequest != null)
                    {
                        existingRequest.Status = "Finished";
                    }

                }

            }
            RepositoryContext.SaveChanges();
        }
    }
}
