
using Entities;
using Entities.Models;
using JomHikeApiV2.Contracts;
using JomHikeApiV2.Entities.ExtendedModels;
using JomHikeApiV2.Entities.Extensions;
using JomHikeApiV2.Entities.Models;
using JomHikeApiV2.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using JomHikeApiV2.Entities.ExtendedModels.User;

namespace Repository
{
    public class EventRepository : RepositoryBase<Event>, IEventRepository
    {
        public EventRepository(RepositoryContext repositoryContext) : base(repositoryContext)
        {
        }

        public void CreateEvent(Event events)
        {
            Create(events);
        }

        public void DeleteEvent(Event events)
        {
            Delete(events);
        }
        public IEnumerable<EventExtended> GetAllEventsWeb()
        {


            return FindAll().OrderByDescending(e => e.StartDate).Select(e => new EventExtended()
            {
                Id = e.Id,
                Name = e.Name,
                Location = e.Location,
                State = e.State,
                OrganizerName = e.OrganizerName,
                OrganizerRegistrationNumber = e.OrganizerRegistrationNumber,
                Description = e.Description,
                StartDate = e.StartDate,
                EndDate = e.EndDate,
                Status = e.Status,
                UserId = e.UserId,
                EventImageUrl = e.EventImageUrl,
                //MountainId = e.MountainId,
                Mountain = RepositoryContext.Mountains.Where(mr => mr.Id.Equals(e.MountainId)).FirstOrDefault()

            }).ToList();
        }
        public int CountEventApproved()
        {
            return FindAll().OrderByDescending(e => e.StartDate).Where(e => e.Status == "Approved").Count();
        }
        public int CountEventPending()
        {
            return FindAll().OrderByDescending(e => e.StartDate).Where(e => e.Status == "Pending").Count();
        }
        public int CountEventRejected()
        {
            return FindAll().OrderByDescending(e => e.StartDate).Where(e => e.Status == "Rejected").Count();
        }
        public IEnumerable<EventExtended> GetAllEvents()
        {


            return FindAll().OrderByDescending(e => e.StartDate).Where(e => e.StartDate >= DateTime.Now && e.Status=="Approved").Select(e => new EventExtended()
            {
                Id = e.Id,
                Name = e.Name,
                Location = e.Location,
                State = e.State,
                OrganizerName = e.OrganizerName,
                OrganizerRegistrationNumber = e.OrganizerRegistrationNumber,
                Description = e.Description,
                StartDate = e.StartDate,
                EndDate = e.EndDate,
                Status = e.Status,
                UserId = e.UserId,
                EventImageUrl = e.EventImageUrl,
                //MountainId = e.MountainId,
                Mountain = RepositoryContext.Mountains.Where(mr => mr.Id.Equals(e.MountainId)).FirstOrDefault()

            }).ToList();
        }

        public Event GetEventById(int eventId)
        {
            return FindByCondition(e => e.Id.Equals(eventId)).Include(events => events.Mountain)
            .Include(events => events.Rides)
            .Include(events => events.UserEvents)
            .ThenInclude(users=>users.User).ToList()
            .FirstOrDefault();
        }
        public IEnumerable<UserJoinedEventExtended>  GetUserJoinByEventId(int eventId)
        {
            return RepositoryContext.UserEvents.Where(userEvent => userEvent.EventId.Equals(eventId)).Select(e => new UserJoinedEventExtended()
            {
                Id = e.User.Id,
                FirstName = e.User.FirstName,
                LastName = e.User.LastName,
                State = e.User.State,
                PhoneNumber = e.User.PhoneNumber
            })
            .ToList();
        }

        public int GetEventIdByRideId(int rideId)
        {
            return RepositoryContext.Rides.Where(rides => rides.Id.Equals(rideId)).Select(e => e.EventId).FirstOrDefault();
        } 

        public UserEvent GetUserEventById(Guid userId, int eventId)
        {
            return RepositoryContext.UserEvents.Where(userEvent => userEvent.UserId.Equals(userId) && userEvent.EventId.Equals(eventId)).FirstOrDefault();
            //return RepositoryContext.Users.Where(u => u.Id.Equals(userId)).Include(user => user.).ThenInclude(vehicles => vehicles.Rides).ToList();
        }

        public void UpdateEvent(Event dbEvent, Event events)
        {
            dbEvent.Map(events);
            Update(dbEvent);
        }
        public void UpdateStatus(Event dbEvent,string status)
        {
            dbEvent.MapUpdateStatus(status);
            Update(dbEvent);
        }


    }
}
