﻿using Entities;
using JomHikeApiV2.Contracts;
using JomHikeApiV2.Entities.Extensions;
using JomHikeApiV2.Entities.Models;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApiV2.Repository
{
    public class UserEventRepository : RepositoryBase<UserEvent>, IUserEventRepository
    {
        public UserEventRepository(RepositoryContext repositoryContext) : base(repositoryContext)
        {
        }

        public void CreateUserEvent(UserEvent userEvent)
        {
            Create(userEvent);
        }

        public void DeleteUserEvent(UserEvent userEvents)
        {
            throw new NotImplementedException();
        }

        public UserEvent GetUserEventById(Guid userId)
        {
            throw new NotImplementedException();
        }

        public void UpdateUserEvent(UserEvent dbEvent, UserEvent events)
        {
            throw new NotImplementedException();
        }
        public void UpdateUserEventStatus(UserEvent dbEvent, UserEvent events, string parameter)
        {
            dbEvent.MapStatus(events, parameter);
            Update(dbEvent);
        }
    }
}
