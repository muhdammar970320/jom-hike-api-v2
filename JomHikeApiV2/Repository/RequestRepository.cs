﻿
using Entities;
using Entities.Models;
using JomHikeApiV2.Contracts;
using JomHikeApiV2.Entities;
using JomHikeApiV2.Entities.ExtendedModels;
using JomHikeApiV2.Entities.ExtendedModels.Request;
using JomHikeApiV2.Entities.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repository
{
    public class RequestRepository : RepositoryBase<Request>, IRequestRepository
    {
        public RequestRepository(RepositoryContext repositoryContext) : base(repositoryContext)
        {
        }

        public void ApproveRequest(Request dbRequest, Request request)
        {
            dbRequest.ApproveRequestMap(request);
            Update(dbRequest);
        }

        public void CancelRequest(Request dbRequest, Request request)
        {
            dbRequest.CancelRequestMap(request);
            Update(dbRequest);
        }

        public void CreateRequest(Request request)
        {
            Create(request);
        }

        public void DeleteRequest(Request request)
        {
            Delete(request);
        }

        public IEnumerable<Request> GetAllRequests()
        {
            return FindAll().OrderBy(r => r.Id).ToList();
        }

        public IEnumerable<RequestWithRideExtended> GetRequestByEventIdAndUserId(int eventId, Guid userId)
        {
            return RepositoryContext.Requests.Where(req => req.UserId.Equals(userId) && req.Ride.EventId.Equals(eventId))
                  .Select(e => new RequestWithRideExtended() {
                      Id = e.Id,
                      EstimatedCost = e.EstimatedCost,
                      FinalTotalCost = e.FinalTotalCost,
                      FinalTotalDistance = e.FinalTotalDistance,
                      TotalDistance = e.TotalDistance,
                      PickupLatitude = e.PickupLatitude,
                      PickupLongitude = e.PickupLongitude,
                      Status = e.Status,
                      PaymentMethod = e.PaymentMethod,
                      PaymentStatus = e.PaymentStatus,
                      Ride = RepositoryContext.Rides.Where(r => r.Id.Equals(e.RideId)).Select(ride => new RideWithDriverDetailsExtended()
                      {
                          Id = ride.Id,
                          FirstName = ride.Vehicle.User.FirstName,
                          LastName = ride.Vehicle.User.LastName,
                          DestinationLatitude = ride.DestinationLatitude,
                          DestinationLongitude = ride.DestinationLongitude,
                          RideStartTime = ride.RideStartTime,
                          TotalDistance = ride.TotalDistance,
                          TotalCost =  ride.TotalCost,
                          SeatAvailable = ride.SeatAvailable,
                          MountainName = ride.Event.Mountain.Name,
                          Vehicle = RepositoryContext.Vehicles.Where(v => v.UserId.Equals(ride.Vehicle.UserId)).Select(vehicle => new VehicleExtended()
                          {
                              Id = vehicle.Id,
                              Name = vehicle.Name,
                              Brand = vehicle.Brand,
                              PlateNumber = vehicle.PlateNumber,
                              User = new UserVehicleExtended()
                              {
                                  Id = vehicle.User.Id,
                                  FirstName = vehicle.User.FirstName,
                                  LastName = vehicle.User.LastName,
                                  PhoneNumber = vehicle.User.PhoneNumber
                              }
                          }).FirstOrDefault()
                      }).FirstOrDefault()
                  }).ToList();
                      
  
        }

        public Request GetRequestById(int requestId)
        {
            return FindByCondition(r => r.Id.Equals(requestId))
            .FirstOrDefault();
        }

        public bool IsRequestExists(Guid userId, int rideId)
        {
            return RepositoryContext.Requests.Any(request => request.RideId.Equals(rideId)&& request.UserId.Equals(userId));
        }
        public void UpdatePaymentRequest(Request dbRequest, Request request)
        {
            dbRequest.PaymentMap(request);
            Update(dbRequest);
        }
        public void UpdateRequest(Request dbRequest, Request request)
        {
            dbRequest.Map(request);
            Update(dbRequest);
        }
    }
}
