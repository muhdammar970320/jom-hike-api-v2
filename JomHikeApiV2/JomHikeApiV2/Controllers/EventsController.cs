﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Entities.Extensions;
using Entities.Models;
using JomHikeApiV2.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace JomHikeApiV2.JomHikeApiV2.Controllers
{
    public class EventsController : Controller
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;

        public EventsController(ILoggerManager logger, IRepositoryWrapper repository)
        {
            _logger = logger;
            _repository = repository;
        }
        public ActionResult Index()
        {
            return View(_repository.Event.GetAllEventsWeb());
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CreateEvent(Event events)
        {
            string fileName;
            string extension = ".jpg";
            try
            {

                if (!ModelState.IsValid)
                {
                    var message = string.Join(" | ", ModelState.Values
                       .SelectMany(v => v.Errors)
                       .Select(e => e.ErrorMessage));

                    _logger.LogError("Invalid event object sent from client." + message);
                    return BadRequest(message);
                }

                //Depending on if you want the byte array or a memory stream, you can use the below. 

                var imageDataByteArray = Convert.FromBase64String(events.EventImageBase64);
                //Follow this tutorial for reference
                //dotnetcoretutorials.com/2018/07/21/uploading-images-in-a-pure-json-api/

                fileName = Guid.NewGuid().ToString() + extension; //Create a new Name 
                                                                  //for the file due to security reasons.
                var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\android\\images\\events");

                if (!Directory.Exists(path))
                { //check if the folder exists;
                    Directory.CreateDirectory(path);
                }
                string file = Path.Combine(path, fileName);
                Debug.WriteLine(file);

                if (imageDataByteArray.Length > 0)
                {
                    using (var stream = new FileStream(file, FileMode.Create))
                    {
                        stream.Write(imageDataByteArray, 0, imageDataByteArray.Length);
                        stream.Flush();
                    }
                }


                Event eventToCreate = new Event
                {
                    Name = events.Name,
                    Location = events.Location,
                    State = events.State,
                    UserId = events.UserId,
                    OrganizerName = events.OrganizerName,
                    OrganizerRegistrationNumber = events.OrganizerRegistrationNumber,
                    Description = events.Description,
                    StartDate = events.StartDate,
                    EndDate = events.EndDate,
                    Status = "Pending",
                    MountainId = events.MountainId,
                    EventImageUrl = fileName

                };
                if (eventToCreate.IsObjectNull())
                {
                    _logger.LogError("Event object sent from client is null.");
                    return BadRequest("Event object is null");
                }

                _repository.Event.CreateEvent(eventToCreate);
                _repository.Save();

                //return CreatedAtRoute("GetEventById", new { id = eventToCreate.Id }, eventToCreate);
                return RedirectToAction("Index", "Events");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside CreateEvent action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPost]
        public bool Delete(int id)
        {
            try
            {
                var events = _repository.Event.GetEventById(id);
                if (events.IsObjectNull())
                {
                    _logger.LogError($"Event with id: {id}, hasn't been found in db.");
                    return false;
                }

                _repository.Event.DeleteEvent(events);
                _repository.Save();

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside DeleteEvent action: {ex.Message}");
                return false;
            }
        }
        [HttpPut("{id}")]
        public ActionResult UpdateEvent(int id,Event events)
        {
            try
            {
                if (events.IsObjectNull())
                {
                    _logger.LogError("Event object sent from client is null.");
                    return BadRequest("Event object is null");
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid events object sent from client.");
                    return BadRequest("Invalid model object");
                }

                var dbEvent = _repository.Event.GetEventById(id);
                if (dbEvent.IsEmptyObject())
                {
                    _logger.LogError($"Event with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                _repository.Event.UpdateEvent(dbEvent, events);
                _repository.Save();

                return RedirectToAction("Index", "Events");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateEvent action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        public ActionResult Update(int id)
        {
            return View(_repository.Event.GetEventById(id));
        }
        public ActionResult ApproveEvent(int id)
        {
            var dbEvent = _repository.Event.GetEventById(id);
            _repository.Event.UpdateStatus(dbEvent, "Approved");
            _repository.Save();

            return RedirectToAction("Index", "Events");
        }
        public ActionResult RejectEvent(int id)
        {
            var dbEvent = _repository.Event.GetEventById(id);
            _repository.Event.UpdateStatus(dbEvent, "Rejected");
            _repository.Save();

            return RedirectToAction("Index", "Events");
        }
        public IActionResult Details(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var events = _repository.Event.GetEventById(id);
            if (events == null)
            {
                return NotFound();
            }

            return View(events);
        }
    }
}