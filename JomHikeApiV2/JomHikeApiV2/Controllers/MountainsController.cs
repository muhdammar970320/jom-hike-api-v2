﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities.Extensions;
using Entities.Models;
using JomHikeApiV2.Contracts;
using JomHikeApiV2.ViewModels.Mountain;
using Microsoft.AspNetCore.Mvc;

namespace JomHikeApiV2.JomHikeApiV2.Controllers
{
    public class MountainsController : Controller
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;

        public MountainsController(ILoggerManager logger, IRepositoryWrapper repository)
        {
            _logger = logger;
            _repository = repository;
        }
        public IActionResult Index()
        {
            return View(_repository.Mountain.GetAllMountainsWeb());
        }
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateMountain(CreateMountainViewModel mountain)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var message = string.Join(" | ", ModelState.Values
                       .SelectMany(v => v.Errors)
                       .Select(e => e.ErrorMessage));
                    _logger.LogError("Invalid mountain object sent from client." + message);
                    return BadRequest("Invalid model object");
                }
                Mountain mountainToCreate = new Mountain
                {
                    Name = mountain.Name,
                    HeightFeet = mountain.HeightFeet,
                    HeightMeter = mountain.HeightMeter,
                    State = mountain.State,
                    DestinationLatitude = mountain.DestinationLatitude,
                    DestinationLongitude = mountain.DestinationLongitude
                };

                if (mountain.IsObjectNull())
                {
                    _logger.LogError("Mountain object sent from client is null.");
                    return BadRequest("Mountain object is null");
                }



                _repository.Mountain.CreateMountain(mountainToCreate);
                _repository.Save();

                return RedirectToAction("Index", "Mountains");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside CreateMountain action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPost]
        public bool Delete(int id)
        {
            try
            {
                var mountain = _repository.Mountain.GetMountainById(id);
                if (mountain.IsObjectNull())
                {
                    _logger.LogError($"Mountain with id: {id}, hasn't been found in db.");
                    return false;
                }

                _repository.Mountain.DeleteMountain(mountain);
                _repository.Save();

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside DeleteMountain action: {ex.Message}");
                return false;
            }
        }
        [HttpPost]
        public ActionResult UpdateMountain(EditMountainViewModel mountain)
        {
            var dbMountain = _repository.Mountain.GetMountainById(mountain.Id);
            if (dbMountain.IsEmptyObject())
            {
                _logger.LogError($"User with id: {mountain.Id}, hasn't been found in db.");
                return NotFound();
            }
            Mountain mountainToUpdate = new Mountain
            {
                Name = mountain.Name,
                HeightFeet = mountain.HeightFeet,
                HeightMeter = mountain.HeightMeter,
                State = mountain.State,
                Status = "Approved",
                DestinationLatitude = mountain.DestinationLatitude,
                DestinationLongitude = mountain.DestinationLongitude
            };

            _repository.Mountain.UpdateMountain(dbMountain, mountainToUpdate);
            _repository.Save();
            return RedirectToAction("Index", "Mountains");
        }
        public ActionResult Update(int id)
        {
            Mountain mountain = _repository.Mountain.GetMountainById(id);
            EditMountainViewModel editMountainViewModel = new EditMountainViewModel
            {
                Id = mountain.Id,
                AverageDifficulty = mountain.AverageDifficulty,
                AverageRating = mountain.AverageRating,
                DestinationLatitude = mountain.DestinationLatitude,
                DestinationLongitude = mountain.DestinationLongitude,
                HeightFeet = mountain.HeightFeet,
                HeightMeter = mountain.HeightMeter,
                Name = mountain.Name,
                State = mountain.State
            };
            return View(editMountainViewModel);
        }
        public ActionResult ApproveMountain(int id)
        {
            var dbMountain = _repository.Mountain.GetMountainById(id);
            _repository.Mountain.UpdateStatus(dbMountain, "Approved");
            _repository.Save();

            return RedirectToAction("Index", "Mountains");
        }
        public ActionResult RejectMountain(int id)
        {
            var dbMountain = _repository.Mountain.GetMountainById(id);
            _repository.Mountain.UpdateStatus(dbMountain, "Rejected");
            _repository.Save();

            return RedirectToAction("Index", "Mountains");
        }
        public IActionResult Details(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var mountain = _repository.Mountain.GetMountainById(id);
            if (mountain == null)
            {
                return NotFound();
            }

            return View(mountain);
        }
    }
}