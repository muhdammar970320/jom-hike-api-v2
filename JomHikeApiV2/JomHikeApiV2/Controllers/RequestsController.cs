﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities.Extensions;
using JomHikeApiV2.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace JomHikeApiV2.JomHikeApiV2.Controllers
{
    public class RequestsController : Controller
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;

        public RequestsController(ILoggerManager logger, IRepositoryWrapper repository)
        {
            _logger = logger;
            _repository = repository;
        }
        public IActionResult Index()
        {
            return View(_repository.Request.GetAllRequests());
        }
        public IActionResult Details(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var request = _repository.Request.GetRequestById(id);
            if (request == null)
            {
                return NotFound();
            }

            return View(request);
        }

        [HttpPost]
        public bool Delete(int id)
        {
            try
            {
                var request = _repository.Request.GetRequestById(id);
                if (request.IsObjectNull())
                {
                    _logger.LogError($"Mountain with id: {id}, hasn't been found in db.");
                    return false;
                }

                _repository.Request.DeleteRequest(request);
                _repository.Save();

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside DeleteMountain action: {ex.Message}");
                return false;
            }
        }

    }
}