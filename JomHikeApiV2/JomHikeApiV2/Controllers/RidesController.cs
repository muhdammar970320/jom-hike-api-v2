﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities.Extensions;
using JomHikeApiV2.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace JomHikeApiV2.JomHikeApiV2.Controllers
{
    public class RidesController : Controller
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;

        public RidesController(ILoggerManager logger, IRepositoryWrapper repository)
        {
            _logger = logger;
            _repository = repository;
        }
        public IActionResult Index()
        {
            return View(_repository.Ride.GetAllRides());
        }
        public IActionResult Details(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ride =  _repository.Ride.GetRideWithRequestByRideId(id);
            if (ride == null)
            {
                return NotFound();
            }

            return View(ride);
        }

        [HttpPost]
        public bool Delete(int id)
        {
            try
            {
                var ride = _repository.Ride.GetRideById(id);
                if (ride.IsObjectNull())
                {
                    _logger.LogError($"Ride with id: {id}, hasn't been found in db.");
                    return false;
                }

                _repository.Ride.DeleteRide(ride);
                _repository.Save();

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside DeleteMountain action: {ex.Message}");
                return false;
            }
        }
    }
}