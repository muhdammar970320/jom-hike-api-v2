﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;
using Entities.Utils;
using JomHikeApiV2.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace JomHikeApiV2.JomHikeApiV2.Controllers
{
    public class AccountController : Controller
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;

        public AccountController(ILoggerManager logger, IRepositoryWrapper repository)
        {
            _logger = logger;
            _repository = repository;
        }
        public IActionResult Login()
        {
            return View();
        }
        public ActionResult Validate(User value)
        {
            //Check exist user in database
            if (_repository.User.IsUserExists(value.UserName))
            {
                //FirstOrDefault tu apa maksudnya
                User user = _repository.User.GetUser(value.UserName);

                //Calculate hash password from data of client and compare with hash in server with salt
                var client_post_hash_password = Convert.ToBase64String(
                      Common.SaltHashPassword(Encoding.ASCII.GetBytes(value.Password)
                    , Convert.FromBase64String(user.Salt)));

                if (client_post_hash_password.Equals(user.Password))
                {
                    if (user.Role.Equals("Admin"))
                    {
                        HttpContext.Session.SetString("userId", user.Id.ToString());
                        HttpContext.Session.SetString("userName", user.UserName);
                        return Json(new { status = true, message = "Login successfully" });
                    }
                    return Json(new { status = false, message = "Sorry you are not administrator" });
                }
                  
                else
                    return Json(new { status = false, message = "Wrong username or password" });

            }
            else
            {
                return Json(new { status = false, message = "User is not existing in database" });
            }
          
        }
        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            return View("Login");
        }
    }
}