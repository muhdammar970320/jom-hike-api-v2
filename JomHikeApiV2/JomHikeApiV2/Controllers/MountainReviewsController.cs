﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities.Extensions;
using JomHikeApiV2.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace JomHikeApiV2.JomHikeApiV2.Controllers
{
    public class MountainReviewsController : Controller
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;

        public MountainReviewsController(ILoggerManager logger, IRepositoryWrapper repository)
        {
            _logger = logger;
            _repository = repository;
        }
        public IActionResult Index()
        {
            return View(_repository.MountainReview.GetAllMountainReviews());
        }
        [HttpPost]
        public bool Delete(int id)
        {
            try
            {
                var review = _repository.MountainReview.GetMountainReviewById(id);
                if (review.IsObjectNull())
                {
                    _logger.LogError($"Mountain with id: {id}, hasn't been found in db.");
                    return false;
                }

                _repository.MountainReview.DeleteMountainReview(review);
                _repository.Save();

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside DeleteMountain action: {ex.Message}");
                return false;
            }
        }

    }
}