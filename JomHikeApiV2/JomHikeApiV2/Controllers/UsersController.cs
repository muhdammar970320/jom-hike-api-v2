﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Extensions;
using Entities.Models;
using Entities.Utils;
using JomHikeApiV2.Contracts;
using JomHikeApiV2.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace JomHikeApiV2.JomHikeApiV2.Controllers
{
    public class UsersController : Controller
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;

        public UsersController(ILoggerManager logger, IRepositoryWrapper repository)
        {
            _logger = logger;
            _repository = repository;
        }
        public ActionResult Index()
        {
            return View(_repository.User.GetAllUsers());
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CreateUser(CreateUserViewModel user)
        {
            try
            {

                if (!_repository.User.IsUserExists(user.UserName))
                {
                    //First, we need check User have exist on database
                    //if (user.IsObjectNull())
                    //{
                    //    _logger.LogError("User object sent from client is null.");
                    //    return BadRequest("User object is null");
                    //}
                    if (!ModelState.IsValid)
                    {
                        var message = string.Join(" | ", ModelState.Values
                      .SelectMany(v => v.Errors)
                      .Select(e => e.ErrorMessage));
                        _logger.LogError("Invalid user object sent from client. " + message);
                        return BadRequest("Invalid model object");
                    }
                    var HashSalt = Convert.ToBase64String(Common.GetRandomSalt(16));
                    User userToCreate = new User()
                    {
                        UserName = user.UserName,//Assign POST to UserName
                        Salt = HashSalt, //Get random salt string
                        Password = Convert.ToBase64String(Common.SaltHashPassword(
                            Encoding.ASCII.GetBytes(user.Password),
                            Convert.FromBase64String(HashSalt))),
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        IdentityNumber = user.IdentityNumber,
                        PhoneNumber = user.PhoneNumber,
                        Role = user.Role,
                        Address1 = user.Address1,
                        Address2 = user.Address2,
                        PostalCode = user.PostalCode,
                        Email = user.Email,
                    };

                    _repository.User.Create(userToCreate);
                    _repository.Save();
                    _logger.LogInfo($"Register succesfully.{userToCreate.Id}");
                    return RedirectToAction("Index", "Users");
                }
                else
                {
                    _logger.LogInfo($"Status Code 422: User already exist. {user.UserName}" + NotFound(" Can register succesfully").StatusCode);
                    //return NotFound("Register Successfully").Value;
                    ModelState.AddModelError("Error", "Check ID");
                    //return StatusCode(422, "User is existing in Database");
                    return RedirectToAction("Index", "Users");
                }

            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, ex.Message);
            }
        }
 
        [HttpPost]
        public bool Delete(Guid id)
        {
            try
            {
                var user = _repository.User.GetUserById(id);
                if (user.IsObjectNull())
                {
                    _logger.LogError($"Event with id: {id}, hasn't been found in db.");
                    return false;
                }

                _repository.User.DeleteUser(user);
                _repository.Save();

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside DeleteEvent action: {ex.Message}");
                return false;
            }
        }
        [HttpPost]
        public ActionResult UpdateUser(User user)
        {
            var dbUser = _repository.User.GetUserById(user.Id);
            if (dbUser.IsEmptyObject())
            {
                _logger.LogError($"User with id: {user.Id}, hasn't been found in db.");
                return NotFound();
            }

            _repository.User.UpdateUser(dbUser, user);
            _repository.Save();
            return RedirectToAction("Index", "Users");
        }
        public ActionResult Update(Guid id)
        {
            return View(_repository.User.GetUserById(id));
        }
        public IActionResult Details(Guid id)
        {
            if (id == null)
            {
                return NotFound();
            }

            
           var user = _repository.User.GetUserByIdWithAllDetails(id);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }
        public ActionResult ApproveUser(Guid id)
        {
            var dbUser = _repository.User.GetUserById(id);
            _repository.User.UpdateStatus(dbUser, 4);
            _repository.Save();

            return RedirectToAction("Index", "Users");
        }
        public ActionResult RejectUser(Guid id)
        {
            var dbUser = _repository.User.GetUserById(id);
            _repository.User.UpdateStatus(dbUser, 2);
            _repository.Save();

            return RedirectToAction("Index", "Users");
        }
        public ActionResult ResetVerificationUser(Guid id)
        {
            var dbUser = _repository.User.GetUserById(id);
            _repository.User.UpdateStatus(dbUser, 3);
            _repository.Save();

            return RedirectToAction("Index", "Users");
        }
    }
}