﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities.Extensions;
using JomHikeApiV2.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace JomHikeApiV2.JomHikeApiV2.Controllers
{
    public class VehiclesController : Controller
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;

        public VehiclesController(ILoggerManager logger, IRepositoryWrapper repository)
        {
            _logger = logger;
            _repository = repository;
        }
        public IActionResult Index()
        {
            return View(_repository.Vehicle.GetAllVehicles());
        }
        public IActionResult Details(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vehicle = _repository.Vehicle.GetVehicleById(id);
            if (vehicle == null)
            {
                return NotFound();
            }

            return View(vehicle);
        }
        [HttpPost]
        public bool Delete(int id)
        {
            try
            {
                var vehicle = _repository.Vehicle.GetVehicleById(id);
                if (vehicle.IsObjectNull())
                {
                    _logger.LogError($"Vehicle with id: {id}, hasn't been found in db.");
                    return false;
                }
                if (_repository.Vehicle.isVehicleExistInRide(id))
                {

                    return false;
                }
                else
                {
                    _repository.Vehicle.DeleteVehicle(vehicle);
                    _repository.Save();

                    return true;
                }


                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside DeleteMountain action: {ex.Message}");
                return false;
            }
        }
    }
}