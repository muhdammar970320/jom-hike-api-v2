﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JomHikeApiV2.Contracts;
using JomHikeApiV2.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace JomHikeApiV2.JomHikeApiV2.Controllers
{
    public class HomeController : Controller
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;

        public HomeController(ILoggerManager logger, IRepositoryWrapper repository)
        {
            _logger = logger;
            _repository = repository;
        }
        public IActionResult Index()
        {
            DashboardViewModel dashboard = new DashboardViewModel();

            dashboard.mountains_count = _repository.Mountain.GetAllMountains().Count();
            dashboard.events_count = _repository.Event.GetAllEventsWeb().Count();
            dashboard.mountain_reviews_count = _repository.MountainReview.GetAllMountainReviews().Count();
            dashboard.requests_count = _repository.Request.GetAllRequests().Count();
            dashboard.rides_count = _repository.Ride.GetAllRides().Count();
            dashboard.users_count = _repository.User.GetAllUsers().Count();
            dashboard.vehicles_count = _repository.Vehicle.GetAllVehicles().Count();

            //Count Mountain
            dashboard.mountain_approved_count = _repository.Mountain.CountMountainApproved();
            dashboard.mountain_pending_count = _repository.Mountain.CountMountainPending();
            dashboard.mountain_rejected_count = _repository.Mountain.CountMountainRejected();

            //Count Event
            dashboard.events_approved_count = _repository.Event.CountEventApproved();
            dashboard.events_pending_count = _repository.Event.CountEventPending();
            dashboard.events_rejected_count = _repository.Event.CountEventRejected();

            return View(dashboard);
        }
    }
}