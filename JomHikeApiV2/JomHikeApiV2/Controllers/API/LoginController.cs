using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;


using Entities.Utils;
using JomHikeApiV2.Contracts;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace JomHikeApi.Controllers
{

    [Route("api/[controller]")]
    public class LoginController : ControllerBase
    {
        private IRepositoryWrapper _repository;
        private ILoggerManager _logger;

        public LoginController(IRepositoryWrapper repository, ILoggerManager logger)
        {
            _repository = repository;
            _logger = logger;
        }

        //POST api/<controller>
        [HttpPost]
        public string Post([FromBody]User value)
        {
            //Check exist user in database
            if (_repository.User.IsUserExists(value.UserName))
            {
                //FirstOrDefault tu apa maksudnya
                User user = _repository.User.GetUser(value.UserName);

                //Calculate hash password from data of client and compare with hash in server with salt
                var client_post_hash_password = Convert.ToBase64String(
                      Common.SaltHashPassword(Encoding.ASCII.GetBytes(value.Password)
                    , Convert.FromBase64String(user.Salt)));

                if (client_post_hash_password.Equals(user.Password))
                    return JsonConvert.SerializeObject(user);
                else
                    return JsonConvert.SerializeObject("Wrong User or Password");

            }
            else
            {
                return JsonConvert.SerializeObject("User is not existing in Database");
            }
        }




    }
}
