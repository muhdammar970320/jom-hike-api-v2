﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Entities.Extensions;
using Entities.Models;
using JomHikeApiV2.Contracts;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace JomHikeApi.Controllers
{
    [Route("api/[controller]")]
    public class MountainController : ControllerBase
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;

        public MountainController(ILoggerManager logger, IRepositoryWrapper repository)
        {
            _logger = logger;
            _repository = repository;
        }


        // GET: api/<controller>
        [HttpGet]
        public IActionResult GetAllMountains()
        {
            try
            {
                var mountains = _repository.Mountain.GetAllMountains();

                _logger.LogInfo($"Returned all requests from database.");
                return base.Ok(new
                {
                    mountains = mountains,
                    message = "Mountain Get Successfully"
                });
                //return Ok(requests);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetAllRequests action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        
        // GET api/<controller>/5
        [HttpGet("{id}", Name = "GetMountainById")]
        public IActionResult GetMountainById(int id)
        {
            try
            {
                var mountain = _repository.Mountain.GetMountainById(id);

                if (mountain == null)
                {
                    _logger.LogError($"Mountain with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned mountain with id: {id}");
                    return Ok(mountain);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetMountainById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        // GET api/<controller>/5
        [HttpGet("{id}/reviews", Name = "GetMountainReviewByMountainId")]
        public IActionResult GetMountainReviewByMountainId(int id)
        {
            try
            {
                var mountainReview = _repository.Mountain.GetMountainReviewByMountainId(id);

                if (mountainReview == null)
                {
                    _logger.LogError($"Mountain with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned mountain with id: {id}");
                    return base.Ok(new
                    {
                        mountainReviews = mountainReview,
                    });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetMountainById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        // POST api/<controller>
        [HttpPost]
        public IActionResult CreateMountain([FromBody]Mountain mountain)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var message = string.Join(" | ", ModelState.Values
                       .SelectMany(v => v.Errors)
                       .Select(e => e.ErrorMessage));
                    _logger.LogError("Invalid mountain object sent from client." + message);
                    return BadRequest("Invalid model object");
                }
                Mountain mountainToCreate = new Mountain
                {
                    Name = mountain.Name,
                    HeightFeet = mountain.HeightFeet,
                    HeightMeter = mountain.HeightMeter,
                    State = mountain.State,
                    DestinationLatitude = mountain.DestinationLatitude,
                    DestinationLongitude = mountain.DestinationLongitude
                };
              
                if (mountain.IsObjectNull())
                {
                    _logger.LogError("Mountain object sent from client is null.");
                    return BadRequest("Mountain object is null");
                }

               

                _repository.Mountain.CreateMountain(mountain);
                _repository.Save();

                return base.Ok(new
                {
                    message = "Mountain Joined Successfully",
                    //events = events,
                });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside CreateMountain action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public IActionResult UpdateMountain(int id, [FromBody]Mountain mountain)
        {
            try
            {
                if (mountain.IsObjectNull())
                {
                    _logger.LogError("Mountain object sent from client is null.");
                    return BadRequest("Mountain object is null");
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid mountain object sent from client.");
                    return BadRequest("Invalid model object");
                }

                var dbMountain = _repository.Mountain.GetMountainById(id);
                if (dbMountain.IsEmptyObject())
                {
                    _logger.LogError($"Mountain with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                _repository.Mountain.UpdateMountain(dbMountain, mountain);
                _repository.Save();

                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateMountain action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }


        [HttpDelete("{id}")]
        public IActionResult DeleteMountain(int id)
        {
            try
            {
                var mountain = _repository.Mountain.GetMountainById(id);
                if (mountain.IsObjectNull())
                {
                    _logger.LogError($"Mountain with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                _repository.Mountain.DeleteMountain(mountain);
                _repository.Save();

                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside DeleteMountain action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
    }
}
