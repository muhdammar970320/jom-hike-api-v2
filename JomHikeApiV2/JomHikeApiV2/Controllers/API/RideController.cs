﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities.Extensions;
using Entities.Models;
using JomHikeApiV2.Contracts;
using JomHikeApiV2.Entities.ExtendedModels;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace JomHikeApi.Controllers
{
    [Route("api/[controller]")]
    public class RideController : ControllerBase
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;

        public RideController(ILoggerManager logger, IRepositoryWrapper repository)
        {
            _logger = logger;
            _repository = repository;
        }

        // GET: api/<controller>
        [HttpGet]
        public IActionResult GetAllRides()
        {
            try
            {
                var rides = _repository.Ride.GetAllRides();

                _logger.LogInfo($"Returned all vehicles from database.");

                return Ok(rides);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetAllVehicle action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // GET api/<controller>/5
        [HttpGet("{id}", Name= "GetRideById")]
        public IActionResult GetRideById(int id)
        {
            try
            {
                var ride = _repository.Ride.GetRideById(id);

                if (ride == null)
                {
                    _logger.LogError($"Ride with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned ride with id: {id}");
                    return Ok(ride);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetRideById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // GET api/<controller>/5
        [HttpGet("event/{eventId}", Name = "GetAllRideByEventId")]
        public IActionResult GetAllRideByEventId(int eventId)
        {
            try
            {
                var rides = _repository.Ride.GetAllRidesByEventId(eventId);

                if (!rides.Any())
                {
                    _logger.LogError($"Ride with id: {eventId}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned ride with id: {eventId}");
                    return base.Ok(new
                    {
                        status = 200,
                        rides = rides,
                    });

                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetRideById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        // GET api/<controller>/5
        [HttpGet("{rideId}/requests", Name = "GetRideWithRequestByRideId")]
        public IActionResult GetRideWithRequestByRideId(int rideId)
        {
            try
            {
                var rides = _repository.Ride.GetRideWithRequestByRideId(rideId);

                if (rides == null)
                {
                    _logger.LogError($"Ride with id: {rideId}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned ride with id: {rideId}");
                    return base.Ok(new
                    {
                        status = 200,
                        rides = rides,
                    });

                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetRideById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        // GET api/<controller>/5
        [HttpGet("event/{eventId}/user/{userId}", Name = "GetRideWithRequest")]
        public IActionResult GetRideWithRequest(Guid userId, int eventId)
        {
            try
            {
                var rides = _repository.Ride.GetRideWithRequest(userId, eventId);

                if (rides == null)
                {
                    _logger.LogError($"Ride with id: {eventId}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned ride with id: {eventId}");
                    return base.Ok(new
                    {
                        status = 200,
                        rides = rides,
                    });

                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetRideById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        // GET api/<controller>/5
        [HttpGet("event/{eventId}/user/{userId}/count", Name = "CountRideRequestByUserId")]
        public IActionResult CountRideRequestByUserId(Guid userId, int eventId)
        {
            try
            {
                var total_request = _repository.Ride.CountRideRequestByUserId(userId, eventId);

                if (total_request == -1)
                {
                    _logger.LogError($"Ride with id: {eventId}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned ride with id: {eventId}");
                    return base.Ok(new
                    {
                        status = 200,
                        total_request = total_request,
                    });

                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside CountRideRequestByUserId action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        // GET api/<controller>/5
        [HttpGet("user/{userId}", Name = "GetRideByUserId")]
        public IActionResult GetRideByUserId(Guid userId)
        {
            try
            {
                var rides = _repository.Ride.GetRideByUserId(userId);

                if (rides == null)
                {
                    _logger.LogError($"Ride with userId: {userId}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned ride with id: {userId}");
                    return base.Ok(new
                    {
                        status = 200,
                        rides = rides,
                    });

                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetRideById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        // POST api/<controller>
        [HttpPost]
        public IActionResult CreateRide([FromBody]Ride ride)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid ride object sent from client.");
                    return BadRequest("Invalid model object");
                }
                if (ride.IsObjectNull())
                {
                    _logger.LogError("Ride object sent from client is null.");
                    return BadRequest("Ride object is null");
                }
                if(_repository.Ride.IsRideExists(ride.VehicleId, ride.EventId))
                {
                    return base.Ok(new
                    {
                        message = "Ride already offered"
                    });
                }
                else
                {
                    var vehicle = _repository.Vehicle.GetVehicleById(ride.VehicleId);
                    var userEvents = _repository.Event.GetUserEventById(vehicle.UserId, ride.EventId);
                    userEvents.IsOfferRide = 1;
                    //_repository.Save();
                    //_repository.UserEvent.UpdateUserEventOfferRideStatus(userEvents,userEvents);
                    _repository.Ride.CreateRide(ride);
                    _repository.Save();
                    return base.Ok(new
                    {
                        ride = ride,
                        message = "Ride Offered Successfully"
                    });
                 
                }
                
                //return CreatedAtRoute("GetRideById", new { id = ride.Id }, ride);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside CreateRide action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public IActionResult UpdateRide(int id, [FromBody]Ride ride)
        {
            try
            {
                if (ride.IsObjectNull())
                {
                    _logger.LogError("Ride object sent from client is null.");
                    return BadRequest("Ride object is null");
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid ride object sent from client.");
                    return BadRequest("Invalid model object");
                }

                var dbRide = _repository.Ride.GetRideById(id);
                if (dbRide.IsEmptyObject())
                {
                    _logger.LogError($"Ride with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                _repository.Ride.UpdateRide(dbRide, ride);
                _repository.Save();

                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateRide action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public IActionResult DeleteRide(int id)
        {
            try
            {
                var ride = _repository.Ride.GetRideById(id);
                if (ride.IsObjectNull())
                {
                    _logger.LogError($"Ride with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                _repository.Ride.DeleteRide(ride);
                _repository.Save();

                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside DeleteRide action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // PUT api/<controller>/5
        [HttpPost("{id}/confirm")]
        public IActionResult ConfirmRide(int id, [FromBody] RequestRideExtended rideModel)
        {
            try
            {
                var rideTest = rideModel;
                //if (ride.IsObjectNull())
                //{
                //    _logger.LogError("Ride object sent from client is null.");
                //    return BadRequest("Ride object is null");
                //}

                //if (!ModelState.IsValid)
                //{
                //    _logger.LogError("Invalid ride object sent from client.");
                //    return BadRequest("Invalid model object");
                //}

                var dbRide = _repository.Ride.GetRideById(id);
                if (dbRide == null)
                {
                    _logger.LogError($"Ride with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                _repository.Ride.ConfirmRide(id, rideModel);
                _repository.Save();
                return base.Ok(new
                {
                    message = "Ride Confirm Successfully"
                });
                //return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateRide action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPost("{id}/payment/cash")]
        public IActionResult PaymentCash(int id, [FromBody] RequestRideExtended rideModel)
        {
            try
            {
                var rideTest = rideModel;
                //if (ride.IsObjectNull())
                //{
                //    _logger.LogError("Ride object sent from client is null.");
                //    return BadRequest("Ride object is null");
                //}

                //if (!ModelState.IsValid)
                //{
                //    _logger.LogError("Invalid ride object sent from client.");
                //    return BadRequest("Invalid model object");
                //}

                var dbRide = _repository.Ride.GetRideById(id);
                if (dbRide == null)
                {
                    _logger.LogError($"Ride with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                _repository.Ride.ConfirmRide(id, rideModel);
                _repository.Save();
                return base.Ok(new
                {
                    message = "Ride Confirm Successfully"
                });
                //return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateRide action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPost("{id}/finish")]
        public IActionResult FinishRide(int id, [FromBody] RequestRideExtended rideModel)
        {
            try
            {
                var rideTest = rideModel;
                //if (ride.IsObjectNull())
                //{
                //    _logger.LogError("Ride object sent from client is null.");
                //    return BadRequest("Ride object is null");
                //}

                //if (!ModelState.IsValid)
                //{
                //    _logger.LogError("Invalid ride object sent from client.");
                //    return BadRequest("Invalid model object");
                //}

                var dbRide = _repository.Ride.GetRideById(id);
                if (dbRide == null)
                {
                    _logger.LogError($"Ride with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                _repository.Ride.FinishRide(id, rideModel);
                _repository.Save();
                return base.Ok(new
                {
                    message = "Ride Finish Successfully"
                });
                //return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateRide action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
    }
}
