﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using Entities.Extensions;
using Entities.Models;
using JomHikeApiV2.Contracts;
using JomHikeApiV2.Entities;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace JomHikeApi.Controllers
{
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private IRepositoryWrapper _repository;
        private ILoggerManager _logger;

        public UserController(IRepositoryWrapper repository, ILoggerManager logger)
        {
            _repository = repository;
            _logger = logger;
        }

        // GET: api/<controller>
        [HttpGet]
        public IActionResult GetAllUsers()
        {
            try
            {
                var users = _repository.User.GetAllUsers();

                _logger.LogInfo($"Returned all vehicles from database.");

                return Ok(users);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetAllUsers action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public IActionResult GetUserById(Guid id)
        {
            try
            {
                var user = _repository.User.GetUserById(id);

                if (user == null)
                {
                    _logger.LogError($"User with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned user with id: {id}");
                    return Ok(user);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetUserById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // GET api/<controller>/5
        [HttpGet("{id}/status")]
        public IActionResult GetStatusUserById(Guid id)
        {
            try
            {
                var user = _repository.User.GetUserById(id);

                if (user == null)
                {
                    _logger.LogError($"User with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned user with id: {id}");
                    return base.Ok(new
                    {
                        userStatus = user.Status,
                        status = 200,
                        message = "Get user status successfully",
                    });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetUserById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }


        [HttpGet("{id}/events/incoming")]
        public IActionResult GetIncomingEventUserById(Guid id)
        {
            try
            {
                var events = _repository.User.GetIncomingEventUserById(id);

                if (events == null)
                {
                    _logger.LogError($"User with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned user with id: {id}");
                    return base.Ok(new
                    {
                        events = events,
                        status = 200,
                        message = "Get user status successfully",
                    });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetEventUserById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpGet("{id}/events/previous")]
        public IActionResult GetPreviousEventUserById(Guid id)
        {
            try
            {
                var events = _repository.User.GetPreviousEventUserById(id);

                if (events == null)
                {
                    _logger.LogError($"User with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned user with id: {id}");
                    return base.Ok(new
                    {
                        events = events,
                        status = 200,
                        message = "Get user status successfully",
                    });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetEventUserById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        [HttpGet("{id}/events/organized")]
        public IActionResult GetOrganizedEventUserById(Guid id)
        {
            try
            {
                var events = _repository.User.GetOrganizedEventUserById(id);

                if (events == null)
                {
                    _logger.LogError($"User with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned user with id: {id}");
                    return base.Ok(new
                    {
                        events = events,
                        status = 200,
                        message = "Get user status successfully",
                    });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetEventUserById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public IActionResult UpdateUser(Guid id, [FromBody]User user)
        {
            try
            {
                //ModelState["Password"].Errors.Clear();
                //ModelState["UserName"].Errors.Clear();
                if (user.IsObjectNull())
                {
                    _logger.LogError("User object sent from client is null.");
                    return BadRequest("User object is null");
                }
                //TryValidateModel(user);
                if (!ModelState.IsValid)
                {
                    var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));

                    return BadRequest(message);
                }

                var dbUser = _repository.User.GetUserById(id);
                if (dbUser.IsEmptyObject())
                {
                    _logger.LogError($"User with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                _repository.User.UpdateUser(dbUser, user);
                _repository.Save();

                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateUser action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPost("{id}/verification")]
        public IActionResult UpdateUserVerification(Guid id, [FromBody]UserExtended userExtended)
        {
            try
            {
           
                //ModelState["Password"].Errors.Clear();
                //ModelState["UserName"].Errors.Clear();
                if (userExtended==null)
                {
                    _logger.LogError("User object sent from client is null.");
                    return BadRequest("User object is null");
                }
                //TryValidateModel(user);
                if (!ModelState.IsValid)
                {
                    var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));
                    _logger.LogError($"Something went wrong inside UpdateUser action: "+message);
                    return BadRequest(message);
                }
              string imageIcFrontUrl =  saveImageIc(userExtended.UserIcFrontImageBase64, id, "ic");
              string imageIcBackUrl = saveImageIc(userExtended.UserIcBackImageBase64, id, "ic");
              string imageSelfieUrl = saveImageIc(userExtended.UserSelfieImageBase64, id, "selfie");

                var dbUser = _repository.User.GetUserById(id);

                if (dbUser.IsEmptyObject())
                {
                    _logger.LogError($"User with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                User user = new User
                {
                    FirstName = userExtended.FirstName,
                    LastName = userExtended.LastName,
                    IdentityNumber = userExtended.IdentityNumber,
                    UserIcBackImageBase64 = userExtended.UserIcBackImageBase64,
                    UserIcFrontImageBase64 = userExtended.UserIcFrontImageBase64,
                    Address1 = userExtended.Address1,
                    Address2 = userExtended.Address2,
                    PostalCode = userExtended.PostalCode,
                    UserIcFrontImageUrl = imageIcFrontUrl,
                    UserIcBackImageUrl = imageIcBackUrl,
                    UserSelfieImageUrl = imageSelfieUrl,
                    State =  userExtended.State
                }; 

                _repository.User.UpdateUserVerification(dbUser, user);
                _repository.Save();

                return base.Ok(new
                {
                    status = 200,
                    message = "Update user successfully",
                });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateUser action: {ex.Message}");
                return StatusCode(500, new { message = "Internal server error" });
            }
        }

        private string saveImageIc(string imageBase64, Guid userId, string fileDirectoryName)
        {
            string fileName;
            string extension = ".jpg";
            //Depending on if you want the byte array or a memory stream, you can use the below. 

            var imageDataByteArrayIcFront = Convert.FromBase64String(imageBase64);
            //Follow this tutorial for reference
            //dotnetcoretutorials.com/2018/07/21/uploading-images-in-a-pure-json-api/

            fileName = Guid.NewGuid().ToString() + extension; //Create a new Name 
                                                              //for the file due to security reasons.
            var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\android\\images\\users\\"+userId+"\\"+ fileDirectoryName);

            if (!Directory.Exists(path))
            { //check if the folder exists;
                Directory.CreateDirectory(path);
            }
            string file = Path.Combine(path, fileName);
            Debug.WriteLine(file);

            if (imageDataByteArrayIcFront.Length > 0)
            {
                using (var stream = new FileStream(file, FileMode.Create))
                {
                    stream.Write(imageDataByteArrayIcFront, 0, imageDataByteArrayIcFront.Length);
                    stream.Flush();
                }
            }
            return fileName;
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public IActionResult DeleteUser(Guid id)
        {
            try
            {
                var user = _repository.User.GetUserById(id);
                if (user.IsObjectNull())
                {
                    _logger.LogError($"User with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                _repository.User.DeleteUser(user);
                _repository.Save();

                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside DeleteUser action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        [HttpGet("{id}/vehicles")]
        public IActionResult GetUserWithVehicles(Guid id)
        {
            try
            {
                var user = _repository.User.GetUserWithVehicles(id);

                if (user.Id.Equals(Guid.Empty))
                {
                    _logger.LogError($"User with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned user with vehicles for id: {id}");
                    return Ok(user);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetUserWithVehicles action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        [HttpGet("{id}/mountainreviews")]
        public IActionResult GetUserWitMountainReviews(Guid id)
        {
            try
            {
                var user = _repository.User.GetUserWithMountainReviews(id);

                if (user.Id.Equals(Guid.Empty))
                {
                    _logger.LogError($"User with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned user with mountain reviews for id: {id}");
                    return Ok(user);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetUserWitMountainReviews action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        [HttpGet("{id}/requests")]
        public IActionResult GetUserWithRequests(Guid id)
        {
            try
            {
                var user = _repository.User.GetUserWithRequest(id);

                if (user.Id.Equals(Guid.Empty))
                {
                    _logger.LogError($"User with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned user with requests for id: {id}");
                    return Ok(user);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetUserWithRequests action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        //[HttpGet("{id}/rides")]
        //public IActionResult GetUserWitRides(Guid id)
        //{
        //    try
        //    {
        //        var user = _repository.User.GetUserWitRides(id);

        //        if (user.FirstOrDefault().Id.Equals(Guid.Empty))
        //        {
        //            _logger.LogError($"User with id: {id}, hasn't been found in db.");
        //            return NotFound();
        //        }
        //        else
        //        {
        //            _logger.LogInfo($"Returned user with requests for id: {id}");
        //            return Ok(user);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError($"Something went wrong inside GetUserWithRequests action: {ex.Message}");
        //        return StatusCode(500, "Internal server error");
        //    }
        //}
    }
}
