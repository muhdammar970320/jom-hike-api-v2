﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Entities.Extensions;
using Entities.Models;
using JomHikeApiV2.Contracts;
using JomHikeApiV2.Entities.ExtendedModels;
using JomHikeApiV2.Entities.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace JomHikeApi.Controllers
{
    [Route("api/[controller]")]
    public class EventController : ControllerBase
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;

        public EventController(ILoggerManager logger, IRepositoryWrapper repository)
        {
            _logger = logger;
            _repository = repository;
        }


        [HttpGet]
        public IActionResult GetAllEvents()
        {

            try
            {
                var events = _repository.Event.GetAllEvents();

                _logger.LogInfo($"Returned all requests from database.");

                return base.Ok(new
                {
                    status = 200,
                    events = events,
                });
                //return Ok(events);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetAllRequests action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        // GET api/<controller>/5
        [HttpGet("{id}", Name = "GetEventById")]
        public IActionResult GetEventById(int id)
        {
            try
            {
                var eventObject = _repository.Event.GetEventById(id);

                if (eventObject == null)
                {
                    _logger.LogError($"Event with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned event with id: {id}");
                    return Ok(eventObject);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetEventById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // GET api/<controller>/5
        [HttpGet("{id}/users", Name = "GetUserJoinByEventId")]
        public IActionResult GetUserJoinByEventId(int id)
        {
            try
            {
                var user = _repository.Event.GetUserJoinByEventId(id);

                if (user == null)
                {
                    _logger.LogError($"Event with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned event with id: {id}");
                    return base.Ok(new
                    {
                        users = user,
                        status = 200,
                        message = "User joined event get successfully",
                    });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetEventById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        // POST api/<controller>

        [HttpPost]
        public IActionResult CreateEvent([FromBody]Event events)
        {
            string fileName;
            string extension = ".jpg";
            try
            { 
                
                if (!ModelState.IsValid)
                {
                    var message = string.Join(" | ", ModelState.Values
                       .SelectMany(v => v.Errors)
                       .Select(e => e.ErrorMessage));

                    _logger.LogError("Invalid event object sent from client." + message);
                    return BadRequest(message);
                }

                //Depending on if you want the byte array or a memory stream, you can use the below. 

                var imageDataByteArray = Convert.FromBase64String(events.EventImageBase64);
                //Follow this tutorial for reference
                //dotnetcoretutorials.com/2018/07/21/uploading-images-in-a-pure-json-api/

                fileName = Guid.NewGuid().ToString() + extension; //Create a new Name 
                                                                  //for the file due to security reasons.
                var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\android\\images\\events");

                if (!Directory.Exists(path))
                { //check if the folder exists;
                    Directory.CreateDirectory(path);
                }
                string file = Path.Combine(path, fileName);
                Debug.WriteLine(file);

                if (imageDataByteArray.Length > 0)
                {
                    using (var stream = new FileStream(file, FileMode.Create))
                    {
                        stream.Write(imageDataByteArray, 0, imageDataByteArray.Length);
                        stream.Flush();
                    }
                }


                Event eventToCreate = new Event
                {
                    Name = events.Name,
                    Location = events.Location,
                    State = events.State,
                    UserId = events.UserId,
                    OrganizerName = events.OrganizerName,
                    OrganizerRegistrationNumber = events.OrganizerRegistrationNumber,
                    Description = events.Description,
                    StartDate = events.StartDate,
                    EndDate = events.EndDate,
                    Status = "Pending",
                    MountainId = events.MountainId,
                    EventImageUrl = fileName

                };
                if (eventToCreate.IsObjectNull())
                {
                    _logger.LogError("Event object sent from client is null.");
                    return BadRequest("Event object is null");
                }

                _repository.Event.CreateEvent(eventToCreate);
                _repository.Save();

                //return CreatedAtRoute("GetEventById", new { id = eventToCreate.Id }, eventToCreate);
                return base.Ok(new
                {
                    status = 200,
                    message = "Create event successfully",
                });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside CreateEvent action: {ex.Message}");
                return StatusCode(500 ,new { message = "Internal server error" });
            }
        }
        [HttpGet("{eventId}/user/{userId}", Name = "GetUserEventById")]
        public IActionResult GetUserEventById(Guid userId, int eventId)
        {
            try
            {
                var eventObject = _repository.Event.GetUserEventById(userId, eventId);
                if (eventObject == null)
                {
                    _logger.LogError($"UserEvent with userId :{userId}  and eventId: {eventId}, hasn't been found in db.");
                    return NotFound(new
                        {
                            message = "User not joined event yet",
                            status = "404"
                        }
                    );
                }
                else
                {
                    var dto = new UserEventExtended()
                    {
                        Id = eventObject.Id,
                        EventId = eventObject.EventId,
                        UserId = eventObject.UserId,
                        IsOfferRide = eventObject.IsOfferRide,
                        IsRequestRide = eventObject.IsRequestRide,
                        MaxAttempt = eventObject.MaxAttempt,
                        Status = eventObject.Status
                    };
                    _logger.LogInfo($"Returned UserEvent with userId :{userId}  and eventId: {eventId},");
                    return Ok(dto);
                }
                
                
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetUserEventById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPost("join")]
        public IActionResult UserJoinEvent([FromBody]UserEvent userEvent)
        {
            try
            {

                if (!ModelState.IsValid)
                {
                    var message = string.Join(" | ", ModelState.Values
                       .SelectMany(v => v.Errors)
                       .Select(e => e.ErrorMessage));

                    _logger.LogError("Invalid userEvent object sent from client.");
                    return BadRequest(message);
                }
                if (_repository.User.IsJoinEvent(userEvent.UserId, userEvent.EventId))
                {
                    var userEvents = _repository.Event.GetUserEventById(userEvent.UserId, userEvent.EventId);

                    //Update table userEvent status to joined
                    _repository.UserEvent.UpdateUserEventStatus(userEvents, userEvent, "Joined");
                    _repository.Save();
                    return base.Ok(new
                    {
                        message = "Event Joined Successfully",
                        //events = events,
                    });
                }
                if (userEvent == null)
                {
                    _logger.LogError("Event object sent from client is null.");
                    return BadRequest("Event object is null");
                }

                var user = _repository.User.GetUserById(userEvent.UserId);
                var events = _repository.Event.GetEventById(userEvent.EventId);
                UserEvent userEventToCreate = new UserEvent
                {
                    UserId = user.Id,
                    EventId = events.Id,
                };
                _repository.UserEvent.CreateUserEvent(userEventToCreate);
                _repository.Save();

                return base.Ok(new
                {
                    message = "Event Joined Successfully",
                    //events = events,
                });

            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside CreateEvent action: {ex.Message}");
                return StatusCode(500, new
                {
                    message = "Internal server error",
                    //events = events,
                });
            }
        }
        [HttpPost("cancel")]
        public IActionResult UserCancelEvent([FromBody]UserEvent userEvent)
        {
            try
            {

                var userEvents = _repository.Event.GetUserEventById(userEvent.UserId,userEvent.EventId);
        
                if (!ModelState.IsValid)
                {
                    var message = string.Join(" | ", ModelState.Values
                       .SelectMany(v => v.Errors)
                       .Select(e => e.ErrorMessage));

                    _logger.LogError("Invalid userEvent object sent from client.");
                    return BadRequest(message);
                }
                if (_repository.User.IsJoinEvent(userEvent.UserId, userEvent.EventId))
                { 
                    //Update table userEvent status to cancel
                    _repository.UserEvent.UpdateUserEventStatus(userEvents, userEvent, "Cancel");
                    _repository.Save();
                    return base.Ok(new
                    {
                        message = "Event Cancel Successfully",
                        //events = events,
                    });
                }

                if (userEvent == null)
                {
                    _logger.LogError("Event object sent from client is null.");
                    return BadRequest("Event object is null");
                }

                return base.BadRequest(new
                {
                    message = "Event cannot cancel",
                    //events = events,
                });

            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside CreateEvent action: {ex.Message}");
                return StatusCode(500, new
                {
                    message = "Internal server error",
                    //events = events,
                });
            }
        }

        [HttpPut("{id}")]
        public IActionResult UpdateEvent(int id, [FromBody]Event events)
        {
            try
            {
                if (events.IsObjectNull())
                {
                    _logger.LogError("Event object sent from client is null.");
                    return BadRequest("Event object is null");
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid events object sent from client.");
                    return BadRequest("Invalid model object");
                }

                var dbEvent = _repository.Event.GetEventById(id);
                if (dbEvent.IsEmptyObject())
                {
                    _logger.LogError($"Event with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                _repository.Event.UpdateEvent(dbEvent, events);
                _repository.Save();

                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateEvent action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteEvent(int id)
        {
            try
            {
                var events = _repository.Event.GetEventById(id);
                if (events.IsObjectNull())
                {
                    _logger.LogError($"Event with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                _repository.Event.DeleteEvent(events);
                _repository.Save();

                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside DeleteEvent action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

    }
}
