﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Entities.Extensions;
using Entities.Models;

using Entities.Utils;
using JomHikeApiV2.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;


namespace JomHikeApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegisterController : ControllerBase
    {
        private IRepositoryWrapper _repository;
        private ILoggerManager _logger;

        public RegisterController(IRepositoryWrapper repository, ILoggerManager logger)
        {
            _repository = repository;
            _logger = logger;
        }


        // POST api/register
        [HttpPost]
        public IActionResult Post([FromBody] User user)
        {
            try
            {
                
                if (!_repository.User.IsUserExists(user.UserName))
                {
                    //First, we need check User have exist on database
                    //if (user.IsObjectNull())
                    //{
                    //    _logger.LogError("User object sent from client is null.");
                    //    return BadRequest("User object is null");
                    //}
                    if (!ModelState.IsValid)
                    {
                        var message = string.Join(" | ", ModelState.Values
                      .SelectMany(v => v.Errors)
                      .Select(e => e.ErrorMessage));
                        _logger.LogError("Invalid user object sent from client. "+ message);
                        return BadRequest("Invalid model object");
                    }
                    var HashSalt = Convert.ToBase64String(Common.GetRandomSalt(16));
                    User userToCreate = new User()
                    {
                        UserName = user.UserName,//Assign POST to UserName
                        Salt = HashSalt, //Get random salt string
                        Password = Convert.ToBase64String(Common.SaltHashPassword(
                            Encoding.ASCII.GetBytes(user.Password),
                            Convert.FromBase64String(HashSalt))),
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        IdentityNumber = user.IdentityNumber,
                        PhoneNumber = user.PhoneNumber,
                        Address1 = user.Address1,
                        Address2 = user.Address2,
                        PostalCode = user.PostalCode,
                        Role = user.Role,
                        Email = user.Email,
                        DriverLicenseExpiredDate =  user.DriverLicenseExpiredDate,
                        DriverLicenseNumber = user.DriverLicenseNumber
                    };

                    _repository.User.Create(userToCreate);
                    _repository.Save();
                    _logger.LogInfo($"Register succesfully.{userToCreate.Id}");
                    return Ok("Register Successfully");
                }
                else
                {
                    _logger.LogInfo($"Status Code 422: User already exist. {user.UserName}" + NotFound(" Can register succesfully").StatusCode);
                    //return NotFound("Register Successfully").Value;
                    return StatusCode(422, "User is existing in Database");
                }

            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, ex.Message);
            }
        }

    }
}
