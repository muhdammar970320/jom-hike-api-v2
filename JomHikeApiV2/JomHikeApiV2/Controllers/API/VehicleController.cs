﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Entities.Extensions;
using Entities.Models;
using JomHikeApiV2.Contracts;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace JomHikeApi
{
    [Route("api/[controller]")]
    public class VehicleController : ControllerBase
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;

        public VehicleController(ILoggerManager logger, IRepositoryWrapper repository)
        {
            _logger = logger;
            _repository = repository;
        }

        // GET: api/<controller>
        [HttpGet]
        public IActionResult GetAllVehicles()
        {
            try
            {
                var vehicles = _repository.Vehicle.GetAllVehicles();

                _logger.LogInfo($"Returned all vehicles from database.");
                return base.Ok(new
                {
                    status = 200,
                    vehicles = vehicles,
                });
                
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetAllVehicle action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // GET api/<controller>/5
        [HttpGet("{id}", Name = "GetVehicleById")]
        public IActionResult GetVehicleById(int id)
        {
            try
            {
                var vehicle = _repository.Vehicle.GetVehicleById(id);

                if (vehicle == null)
                {
                    _logger.LogError($"Vehicle with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned vehicle with id: {id}");
                    return Ok(vehicle);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetVehicleById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // POST api/<controller>
        [HttpPost]
        public IActionResult CreateVehicle([FromBody]Vehicle vehicle)
        {
            try
            {
                Vehicle vehicleToCreate = new Vehicle
                {
                    Name = vehicle.Name,
                    Brand = vehicle.Brand,
                    MakeYear = vehicle.MakeYear,
                    Color = vehicle.Color,
                    PlateNumber = vehicle.PlateNumber,
                    UserId = vehicle.UserId
                };
                if (!ModelState.IsValid)
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    _logger.LogError("Invalid vehicle object sent from client." + message);
                    return BadRequest("Invalid model object");
                }
                if (vehicleToCreate.IsObjectNull())
                {
                    _logger.LogError("Vehicle object sent from client is null.");
                    return BadRequest("Vehicle object is null");
                }

                _repository.Vehicle.CreateVehicle(vehicleToCreate);
                _repository.Save();
                return base.Ok(new
                {
                    vehicle = vehicleToCreate,
                    message = "Vehicle Add Successfully"
                });
                //return CreatedAtRoute("GetVehicleById", new { id = vehicleToCreate.Id }, vehicleToCreate);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside CreateVehicle action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }


        [HttpPost("{id}")]
        public IActionResult UpdateVehicle(int id, [FromBody]Vehicle vehicle)
        {
            try
            {
                if (vehicle.IsObjectNull())
                {
                    _logger.LogError("Vehicle object sent from client is null.");
                    return BadRequest("Vehicle object is null");
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid vehicle object sent from client.");
                    return BadRequest("Invalid model object");
                }

                var dbVehicle = _repository.Vehicle.GetVehicleById(id);
                if (dbVehicle.IsEmptyObject())
                {
                    _logger.LogError($"Vehicle with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                _repository.Vehicle.UpdateVehicle(dbVehicle, vehicle);
                _repository.Save();

                return base.Ok(new
                {
                    message = "Vehicle Update Successfully"
                });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateVehicle action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public IActionResult DeleteVehicle(int id)
        {
            try
            {
                var vehicle = _repository.Vehicle.GetVehicleById(id);
                if (vehicle.IsObjectNull())
                {
                    _logger.LogError($"Request with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                if (_repository.Vehicle.isVehicleExistInRide(id))
                {

                    return base.Ok(new
                    {
                        message = "Cannot delete the vehicle because it exist on other ride"
                    });
                }
                else
                {
                    _repository.Vehicle.DeleteVehicle(vehicle);
                    _repository.Save();

                    return base.Ok(new
                    {
                        message = "Vehicle delete Successfully"
                    });
                }
            

            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside DeleteVehicle action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
      
    }
}
