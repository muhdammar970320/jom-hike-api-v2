﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using JomHikeApiV2.Contracts;
using JomHikeApiV2.Entities.Models;
using JomHikeApiV2.Entities.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Web;
using System.Drawing;
using System.Diagnostics;

namespace JomHikeApiV2.JomHikeApiV2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImagesController : ControllerBase
    {
        private readonly IImageHandler _imageHandler;
        private ILoggerManager _logger;
        public ImagesController(IImageHandler imageHandler, ILoggerManager logger)
        {
            _logger = logger;
            _imageHandler = imageHandler;
        }

        /// <summary>
        /// Uplaods an image to the server.
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        /// 
        [HttpPost]
        [DisableRequestSizeLimit]
        public async Task<IActionResult> UploadImage(IFormFile file)
        {
            try
            {

                return await _imageHandler.UploadImage(file);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return await _imageHandler.UploadImage(file);

            }
         
        }

        [HttpPost("android")]
        [DisableRequestSizeLimit]
        public IActionResult UploadCustomerImage([FromBody] UploadImageModel model)
        {
            string fileName;
            string extension = ".jpg";
            try
            {
                //Depending on if you want the byte array or a memory stream, you can use the below. 
                _logger.LogInfo(model.ImageData);
                var imageDataByteArray = Convert.FromBase64String(model.ImageData);
                //Follow this tutorial for reference
                //dotnetcoretutorials.com/2018/07/21/uploading-images-in-a-pure-json-api/

                fileName = Guid.NewGuid().ToString() + extension; //Create a new Name 
                                                                  //for the file due to security reasons.
                var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\android\\images\\profile");

                if (!Directory.Exists(path))
                { //check if the folder exists;
                    Directory.CreateDirectory(path);
                }
                string file = Path.Combine(path, fileName);
                Debug.WriteLine(file);

                if (imageDataByteArray.Length > 0)
                {
                    using (var stream = new FileStream(file, FileMode.Create))
                    {
                        stream.Write(imageDataByteArray, 0, imageDataByteArray.Length);
                        stream.Flush();
                    }
                }
                return base.Ok(new
                {
                    message = "Image Upload Successfully",
                    //events = events,
                });
            }
            catch (Exception e)
            {

                _logger.LogError(e.Message);
                return BadRequest();
            }
           
            //return File(imageDataByteArray, "image/png");
        }
    }
}