﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Entities.Extensions;
using Entities.Models;
using JomHikeApiV2.Contracts;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace JomHikeApi.Controllers
{
    [Route("api/[controller]")]
    public class RequestController : ControllerBase
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;

        public RequestController(ILoggerManager logger, IRepositoryWrapper repository)
        {
            _logger = logger;
            _repository = repository;
        }

        // GET: api/<controller>
        [HttpGet]
        public IActionResult GetAllRequests()
        {
            try
            {
                var requests = _repository.Request.GetAllRequests();

                _logger.LogInfo($"Returned all requests from database.");

                return Ok(requests);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetAllRequests action: {ex.Message}");
                return StatusCode(500, $"Internal server error  {ex.Message} ");
            }
        }

        [HttpGet("{id}", Name = "GetRequestById")]
        public IActionResult GetRequestById(int id)
        {
            try
            {
                var request = _repository.Request.GetRequestById(id);

                if (request == null)
                {
                    _logger.LogError($"Request with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned request with id: {id}");
                    return Ok(request);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetRequestById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpGet("event/{eventId}/user/{userId}", Name = "GetRequestByEventIdAndUserId")]
        public IActionResult GetRequestByEventIdAndUserId(int eventId, Guid userId)
        {
            try
            {
                var request = _repository.Request.GetRequestByEventIdAndUserId(eventId, userId);

                if (request == null)
                {
                    _logger.LogError($"Request with userId: {userId}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Request with userId: {userId}");
                    return base.Ok(new
                    {
                        status = 200,
                        message = "Successfully get user request",
                        //events = events,
                        requests = request
                    });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetRequestById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        [HttpPost("check", Name = "IsRequestExists")]
        public IActionResult IsRequestExists([FromBody]Request request)
        {
            try
            {
                var result = _repository.Request.IsRequestExists(request.UserId, request.RideId);

                if (result == null)
                {
                    _logger.LogError($"UserId with id: {request.UserId} and RideId with id :{request.RideId}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned request with userId: {request.UserId} and rideId: {request.RideId} is exists");
                    return base.Ok(new
                    {
                        status = 200,
                        message = "Check Exist Request Successfully",
                        //events = events,
                        requestIsExist = result
                    });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetRequestById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        //[HttpGet("{requestId}/ride/{rideId}", Name = "GetRequestByRideId")]
        //public IActionResult GetRequestByRideId(int requestId, int rideId)
        //{
        //    try
        //    {
        //        var request = _repository.Request.GetRequestByRideId(id);

        //        if (request == null)
        //        {
        //            _logger.LogError($"Request with id: {id}, hasn't been found in db.");
        //            return NotFound();
        //        }
        //        else
        //        {
        //            _logger.LogInfo($"Returned request with id: {id}");
        //            return Ok(request);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError($"Something went wrong inside GetRequestById action: {ex.Message}");
        //        return StatusCode(500, "Internal server error");
        //    }
        //}

        // POST api/<controller>
        [HttpPost]
        public IActionResult CreateRequest([FromBody]Request request)
        {
            try
            {
                //User User = _repository.User.GetUserById(request.User.Id);
                //Ride Ride = _repository.Ride.GetRideById(request.Ride.Id);
                Request requestToCreate = new Request
                {
                    PickupLongitude = request.PickupLongitude,//Assign POST to UserName
                    PickupLatitude = request.PickupLatitude, //Get random salt string
                    RideId = request.RideId,
                    UserId = request.UserId,
                    EstimatedCost = request.EstimatedCost,
                    TotalDistance = request.TotalDistance
                };
               var eventId = _repository.Event.GetEventIdByRideId(request.RideId);
                var userEvents = _repository.Event.GetUserEventById(request.UserId, eventId);
                userEvents.IsRequestRide = 1;
                //if (request.IsObjectNull())
                //{
                //    _logger.LogError("Request object sent from client is null.");
                //    return BadRequest("Request object is null");
                //}

                if (!ModelState.IsValid)
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));

                    _logger.LogError("Invalid request object sent from client.");
                    //return BadRequest("Invalid model object");
                    return BadRequest(message);

                }

                _repository.Request.CreateRequest(requestToCreate);
                _repository.Save();
                return base.Ok(new
                {
                    message = "Request Ride Successfully",
                    //events = events,
                    request = requestToCreate
                });
                //return CreatedAtRoute("GetRequestById", new { id = requestToCreate.Id }, requestToCreate);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside CreateRequest action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // PUT api/<controller>/5
        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public IActionResult UpdateRequest(int id, [FromBody]Request request)
        {
            try
            {
                if (request.IsObjectNull())
                {
                    _logger.LogError("Request object sent from client is null.");
                    return BadRequest("Request object is null");
                }

                if (!ModelState.IsValid)
                {
                    var message = string.Join(" | ", ModelState.Values
                       .SelectMany(v => v.Errors)
                       .Select(e => e.ErrorMessage));

                    _logger.LogError("Invalid request object sent from client.");
                    //return BadRequest("Invalid model object");
                    return BadRequest(message);
                }

                var dbRequest = _repository.Request.GetRequestById(id);
                if (dbRequest.IsEmptyObject())
                {
                    _logger.LogError($"Request with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                _repository.Request.UpdateRequest(dbRequest, request);
                _repository.Save();

                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateRequest action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        [HttpPut("{id}/cancel")]
        public IActionResult CancelRequest(int id, [FromBody]Request request)
        {
            try
            {
                if (request.IsObjectNull())
                {
                    var message = string.Join(" | ", ModelState.Values
                               .SelectMany(v => v.Errors)
                               .Select(e => e.ErrorMessage));

                    _logger.LogError("Invalid request object sent from client.");
                    //return BadRequest("Invalid model object");
                    return BadRequest(message);
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid request object sent from client.");
                    return BadRequest("Invalid model object");
                }

                var dbRequest = _repository.Request.GetRequestById(id);
                if (dbRequest.IsEmptyObject())
                {
                    _logger.LogError($"Request with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                _repository.Request.CancelRequest(dbRequest, request);
                _repository.Save();

                //return NoContent();
                return base.Ok(new
                {
                    status = 200,
                    message = "Cancel successfully",
                });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateRequest action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        [HttpPut("{id}/approved")]
        public IActionResult ApproveRequest(int id, [FromBody]Request request)
        {
            try
            {
                if (request.IsObjectNull())
                {
                    _logger.LogError("Request object sent from client is null.");
                    return BadRequest("Request object is null");
                }

                if (!ModelState.IsValid)
                {
                    var message = string.Join(" | ", ModelState.Values
                                      .SelectMany(v => v.Errors)
                                      .Select(e => e.ErrorMessage));

                    _logger.LogError("Invalid request object sent from client.");
                    //return BadRequest("Invalid model object");
                    return BadRequest(message);
                }

                var dbRequest = _repository.Request.GetRequestById(id);
                if (dbRequest.IsEmptyObject())
                {
                    _logger.LogError($"Request with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                _repository.Request.ApproveRequest(dbRequest, request);
                _repository.Save();
           
                //return NoContent();
                return base.Ok(new
                {
                    status = 200,
                    message = "Approve successfully",
                });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateRequest action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        [HttpPut("{id}/payment")]
        public IActionResult PaymentRequest(int id, [FromBody]Request request)
        {
            try
            {
                if (request.IsObjectNull())
                {
                    _logger.LogError("Request object sent from client is null.");
                    return BadRequest("Request object is null");
                }

                if (!ModelState.IsValid)
                {
                    var message = string.Join(" | ", ModelState.Values
                                      .SelectMany(v => v.Errors)
                                      .Select(e => e.ErrorMessage));

                    _logger.LogError("Invalid request object sent from client.");
                    //return BadRequest("Invalid model object");
                    return BadRequest(message);
                }

                var dbRequest = _repository.Request.GetRequestById(id);
                if (dbRequest.IsEmptyObject())
                {
                    _logger.LogError($"Request with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                _repository.Request.UpdatePaymentRequest(dbRequest, request);
                _repository.Save();

                //return NoContent();
                return base.Ok(new
                {
                    status = 200,
                    message = "Pay successfully",
                });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateRequest action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        // DELETE api/<controller>/5
        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public IActionResult DeleteRequest(int id)
        {
            try
            {
                var request = _repository.Request.GetRequestById(id);
                if (request.IsObjectNull())
                {
                    _logger.LogError($"Request with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                _repository.Request.DeleteRequest(request);
                _repository.Save();

                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside DeleteRequest action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
    }
}
