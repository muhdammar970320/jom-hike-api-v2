﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Entities.Extensions;
using Entities.Models;
using JomHikeApiV2.Contracts;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace JomHikeApi.Controllers
{
    [Route("api/[controller]")]
    public class MountainReviewController : Controller
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;

        public MountainReviewController(ILoggerManager logger, IRepositoryWrapper repository)
        {
            _logger = logger;
            _repository = repository;
        }

        // GET: api/<controller>
        [HttpGet]
        public IActionResult GetAllMountainReviews()
        {
            try
            {
                var mountainReviews = _repository.MountainReview.GetAllMountainReviews();

                _logger.LogInfo($"Returned all mountain reviews from database.");

                return Ok(mountainReviews);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetAllMountainReviews action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        // GET api/<controller>/5
        [HttpGet("{id}",Name = "GetMountainReviewById")]
        public IActionResult GetMountainReviewById(int id)
        {
            try
            {
                var mountainReview = _repository.MountainReview.GetMountainReviewById(id);

                if (mountainReview == null)
                {
                    _logger.LogError($"MountainReview with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo($"Returned mountain review with id: {id}");
                    return Ok(mountainReview);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetMountainReviewById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }



        // POST api/<controller>
        [HttpPost]
        public IActionResult CreateMountainReview([FromBody]MountainReview mountainReview)
        {
            try
            {
                var mountainId = mountainReview.MountainId;
                if (!ModelState.IsValid)
                {
                    var message = string.Join(" | ", ModelState.Values
                       .SelectMany(v => v.Errors)
                       .Select(e => e.ErrorMessage));

                    _logger.LogError(message);
                    return BadRequest(message);
                }
                
               
             

                if (mountainReview.IsObjectNull())
                {
                    _logger.LogError("MountainReview object sent from client is null.");
                    return BadRequest("MountainReview object is null");
                }
               

                if (_repository.MountainReview.IsReviewExists(mountainReview.UserId, mountainId))
                {
                    return base.Ok(new
                    {
                        message = "Review already added",
                        //events = events,
                    });
                }
            
                _repository.MountainReview.CreateMountainReview(mountainReview);
                _repository.Mountain.UpdateMountainRatingAndDifficulty(mountainId, mountainReview.MountainDifficulty, mountainReview.MountainRating);
                _repository.Save();



                return base.Ok(new
                {
                    message = "Review add Successfully",
                    //events = events,
                });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside CreateMountainReview action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public IActionResult UpdateMountainReview(int id, [FromBody]MountainReview mountainReview)
        {
            try
            {
                if (mountainReview.IsObjectNull())
                {
                    _logger.LogError("MountainReview object sent from client is null.");
                    return BadRequest("MountainReview object is null");
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid mountainReview object sent from client.");
                    return BadRequest("Invalid model object");
                }

                var dbMountaReview = _repository.MountainReview.GetMountainReviewById(id);
                if (dbMountaReview.IsEmptyObject())
                {
                    _logger.LogError($"MountainReview with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                _repository.MountainReview.UpdateMountainReview(dbMountaReview, mountainReview);
                _repository.Save();

                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateMountainReview action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public IActionResult DeleteMountainReview(int id)
        {
            try
            {
                var mountainReview = _repository.MountainReview.GetMountainReviewById(id);
                if (mountainReview.IsObjectNull())
                {
                    _logger.LogError($"MountainReview with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                _repository.MountainReview.DeleteMountainReview(mountainReview);
                _repository.Save();

                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside DeleteMountainReview action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
       
    }
}
