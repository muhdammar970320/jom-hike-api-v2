﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities.Models;
using JomHikeApiV2.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Stripe;

namespace JomHikeApiV2.JomHikeApiV2.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class StripePaymentController : ControllerBase
    {

        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;

        public StripePaymentController(ILoggerManager logger, IRepositoryWrapper repository)
        {
            _logger = logger;
            _repository = repository;
        }
        [HttpGet("key")]
        public IActionResult GetKey()
        {
            return base.Ok(new
            {
                publishableKey = "pk_test_3WvOAEAmDn2HfbIr4gS5vRus00JNngilVR"
            });
        }
        [HttpPost]
        public IActionResult Post([FromBody]StripePaymentRequest paymentRequest)
        {
            try
            {
                var intent = new PaymentIntent();
                if (paymentRequest.PaymentMethodId != null)
                {
                    Dictionary<string, string> Metadata = new Dictionary<string, string>();
                    Metadata.Add("Amount", "RM "+(paymentRequest.Amount/100));
                    Metadata.Add("Recipient Email", paymentRequest.Email);
                    Metadata.Add("User Id", paymentRequest.UserId);
                    var service = new PaymentIntentService();
                    var options = new PaymentIntentCreateOptions
                    {
                        UseStripeSdk = true,
                        Amount = paymentRequest.Amount,
                        Currency = "myr",
                        Confirm = true,
                        ConfirmationMethod = "manual",
                        PaymentMethod = paymentRequest.PaymentMethodId,
                        Metadata = Metadata,
                        ReceiptEmail = paymentRequest.Email,
                        Customer = "cus_GVf5bat4a2NZbj"
                    };
                    intent = service.Create(options);

                }
                //else if (paymentRequest.paymentIntentId!=null)
                //{
                //    var service = new PaymentIntentService();
                //   intent = service.Get(paymentRequest.paymentIntentId);
                //   service.Confirm(intent.PaymentMethodId);
                //}
                var request = new Request()
                {
                    PaymentMethod = paymentRequest.PaymentMethod,
                    PaymentStatus = "Credit Card",
                };
                var dbRequest = _repository.Request.GetRequestById(paymentRequest.Id);
                _repository.Request.UpdatePaymentRequest(dbRequest, request);
                _repository.Save();
                var Output = generateResponse(intent);

                return Ok(Output);
            }
            catch (StripeException e)
            {

                return base.Ok(new
                {
                 error =  e.StripeError.Message
                });
            }
          
        }

        private string generateResponse(PaymentIntent intent)
        {
            string Json="";
            switch (intent.Status)
            {
                case "require_action":
                case "require_source_action":
                   Json = JsonConvert.SerializeObject(new
                    {
                        requiresAction = true,
                        paymentIntentId = intent.Id,
                        clientSecret = intent.ClientSecret
                    });
                    break;
                case "requires_payment_method":
                case "requires_source":
                    Json = JsonConvert.SerializeObject(new
                    {
                        error = "Your card was denied, please provide a new payment method"
                    }); ;
                    break;
                case "succeeded":
                    //createCustomer(intent);

                    Json = JsonConvert.SerializeObject( new
                    {
                        clientSecret = intent.ClientSecret
                    });
                    break;
                default:
                    Console.WriteLine("Default case");
                    break;
            }
            return Json;
        }

        private void createCustomer(PaymentIntent intent)
        {
            var options = new CustomerCreateOptions
            {
                Email = intent.ReceiptEmail,
                PaymentMethod = intent.PaymentMethodId,
                InvoiceSettings = new CustomerInvoiceSettingsOptions
                {
                    DefaultPaymentMethod = intent.PaymentMethodId,
                },
            };
            var service = new CustomerService();
            Customer customer = service.Create(options);
        }
        private Customer createCustomer(string PaymentMethodId, string email)
        {
            var options = new CustomerCreateOptions
            {
                Email = email,
                PaymentMethod = PaymentMethodId,
                InvoiceSettings = new CustomerInvoiceSettingsOptions
                {
                    DefaultPaymentMethod = PaymentMethodId,
                },
            };
            var service = new CustomerService();
            Customer customer = service.Create(options);

            return customer;
        }
        public class StripePaymentRequest
        {
            public string TokenId { get; set; }
            public string ProductName { get; set; }
            public int Amount { get; set; }
            public string PaymentMethodId { get; set; }
            public int Id { get; set; }
            public int PaymentMethod { get; set; }
            public string PaymentIntentId  { get; set; }
            public string Email { get; set; }
            public string UserId { get; set; }
        }
    }
}