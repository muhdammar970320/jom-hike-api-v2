﻿
using Entities;
using JomHikeApiV2.Contracts;
using JomHikeApiV2.Entities.Models;
using JomHikeApiV2.Entities.Utils;
using LoggerService;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JomHikeApi.Extensions
{
    /*
     *          REGISTER YOUR CONFIGURATION HERE
     *          EX EMAIL, ETC
     * 
     * 
     *
     **/
    public static class ServiceExtensions
    {
        public static void ConfigureCors(this IServiceCollection services)
        {
            services.AddCors(options =>
            {
            options.AddPolicy("CorsPolicy",
               builder => builder
                .AllowAnyOrigin()
                .AllowAnyHeader()
                .AllowAnyMethod()
                .AllowCredentials());
            //builder => builder.WithOrigins("http://localhost:5000/"));


        });
        }
        public static void ConfigureIISIntegration(this IServiceCollection services)
        {
            services.Configure<IISOptions>(options =>
            {

            });
        }
        public static void ConfigureLoggerService(this IServiceCollection services)
        {
            services.AddSingleton<ILoggerManager, LoggerManager>();
        }
        public static void ConfigureImagesService(this IServiceCollection services)
        {
            services.AddTransient<IImageHandler, ImageHandler>();
            services.AddTransient<IImageWriter,ImageWriter>();
        }
        public static void ConfigurePostgresqlContext(this IServiceCollection services, IConfiguration config)
        {
            //Register database connection
            //var connectionString = config["ConnectionStrings:JomHikeApiConnection"];
            // services.AddEntityFrameworkNpgsql().AddDbContext<RepositoryContext>(opt =>
            //opt.UseNpgsql(connectionString, b => b.MigrationsAssembly("JomHikeApi")).EnableSensitiveDataLogging());

            services.AddEntityFrameworkNpgsql().AddDbContext<RepositoryContext>(options =>
            {
                var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
                string connStr;
                // Depending on if in development or production, use either Heroku-provided
                // connection string, or development connection string from env var.
                if (env == "Development")
                {
                    // Use connection string from file.
                    connStr = config["ConnectionStrings:JomHikeApiV2Connection"]; ;
                }
                else if (env == "Staging")
                {
                    //var connUrl = "postgres://gmtvxbag:THNjM7hnm05Lh4PeRTUjACWSYyA3iNyR@salt.db.elephantsql.com:5432/gmtvxbag";
                    var connUrl = "postgres://ypbxjbkzceyrlx:65ab472b29677acaa1c0eefdc3463a89968b4032c57ace64dbc47ee24af6540f@ec2-54-83-33-14.compute-1.amazonaws.com:5432/d65e8f36lp1iir";
                    // Parse connection URL to connection string for Npgsql
                    connUrl = connUrl.Replace("postgres://", string.Empty);
                    var pgUserPass = connUrl.Split("@")[0];
                    var pgHostPortDb = connUrl.Split("@")[1];
                    var pgHostPort = pgHostPortDb.Split("/")[0];
                    var pgDb = pgHostPortDb.Split("/")[1];
                    var pgUser = pgUserPass.Split(":")[0];
                    var pgPass = pgUserPass.Split(":")[1];
                    var pgHost = pgHostPort.Split(":")[0];
                    var pgPort = pgHostPort.Split(":")[1];
                    connStr = $"Server={pgHost};Port={pgPort};User Id={pgUser};Password={pgPass};Database={pgDb};SslMode=Require" +
                    $"";
                }
                else
                {
                    //connStr = config["ConnectionPostgresql:JomHikeApiV2Connection"]; ;
                    // Use connection string provided at runtime by Heroku.
                    var connUrl = Environment.GetEnvironmentVariable("DATABASE_URL");
                    // Parse connection URL to connection string for Npgsql
                    connUrl = connUrl.Replace("postgres://", string.Empty);
                    var pgUserPass = connUrl.Split("@")[0];
                    var pgHostPortDb = connUrl.Split("@")[1];
                    var pgHostPort = pgHostPortDb.Split("/")[0];
                    var pgDb = pgHostPortDb.Split("/")[1];
                    var pgUser = pgUserPass.Split(":")[0];
                    var pgPass = pgUserPass.Split(":")[1];
                    var pgHost = pgHostPort.Split(":")[0];
                    var pgPort = pgHostPort.Split(":")[1];
                    connStr = $"Server={pgHost};Port={pgPort};User Id={pgUser};Password={pgPass};Database={pgDb}";
                }
                // Whether the connection string came from the local development configuration file
                // or from the environment variable from Heroku, use it to set up your DbContext.
                options.UseNpgsql(connStr);
            });
        }

        public static void ConfigureRepositoryWrapper(this IServiceCollection services)
        {
            services.AddScoped<IRepositoryWrapper, RepositoryWrapper>();
        }

    }
}
